package com.dwi.dwiskripsi;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.dwi.dwiskripsi.admin.unit.DataUnitActivity;
import com.dwi.dwiskripsi.admin.unit.DetailUnitActivity;
import com.dwi.dwiskripsi.admin.unit.EditUnitActivity;
import com.dwi.dwiskripsi.admin.unit.TambahUnitActivity;
import com.dwi.dwiskripsi.model.Unit;
import com.google.android.gms.maps.model.LatLng;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.dwi.dwiskripsi.Config.GETUNIT;
import static com.dwi.dwiskripsi.Config.HAPUSUNIT;
import static com.dwi.dwiskripsi.Config.URL_FOTO_UNIT;

public class TerdekatActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private UnitAdapter adapter;
    private ArrayList<Unit> unitList = new ArrayList<>();
    private SwipeRefreshLayout swipe;
    private Loading load;
    private TextView nodata;
    private SearchView sv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terdekat);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("Tempat Les Terdekat");

        sv = (SearchView) findViewById(R.id.seacrh);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        nodata = (TextView) findViewById(R.id.nodata);
        swipe = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh);
        load = new Loading(this);

        adapter = new UnitAdapter(unitList);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);

        sv.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                adapter.getFilter().filter(newText);
                return false;
            }
        });

        swipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getUnit();
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        getUnit();
    }

    public void getUnit() {
        load.showpDialog();
        JsonObjectRequest jsonReq = new JsonObjectRequest(Request.Method.GET, GETUNIT, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("ResponseUnit", response.toString());
                if (response != null) {
                    try {
                        unitList.clear();
                        String status = response.getString("status");
                        if (status.equals("0")) {
                            nodata.setVisibility(View.VISIBLE);
                            nodata.setText(getString(R.string.nodata));
                        } else {
                            nodata.setVisibility(View.GONE);
                            try {
                                JSONArray feedArray = response.getJSONArray("data");

                                for (int i = 0; i < feedArray.length(); i++) {
                                    JSONObject data = (JSONObject) feedArray.get(i);

                                    String id_unit = data.has("id_unit") ? data.getString("id_unit") : "";
                                    String nama_unit = data.has("nama_unit") ? data.getString("nama_unit") : "";
                                    String alamat = data.has("alamat") ? data.getString("alamat") : "";
                                    String telp = data.has("telp") ? data.getString("telp") : "";
                                    String gambar = data.has("gambar") ? data.getString("gambar") : "";
                                    String url = data.has("url") ? data.getString("url") : "";
                                    String deskripsi = data.has("deskripsi") ? data.getString("deskripsi") : "";
                                    String latitude = data.has("latitude") ? data.getString("latitude") : "";
                                    String longitude = data.has("longitude") ? data.getString("longitude") : "";
                                    String id_login = data.has("id_login") ? data.getString("id_login") : "";
                                    String id_kecamatan = data.has("id_kecamatan") ? data.getString("id_kecamatan") : "";
                                    String nama_kecamatan = data.has("nama_kecamatan") ? data.getString("nama_kecamatan") : "";
                                    String username = data.has("username") ? data.getString("username") : "";
                                    String password = data.has("password") ? data.getString("password") : "";

                                    double latitude1;
                                    double longitude1;

                                    try {
                                        longitude1 = Double.parseDouble(longitude);
                                    } catch (NumberFormatException e) {
                                        longitude1 = 0.00;
                                    }

                                    try {
                                        latitude1 = Double.parseDouble(latitude);
                                    } catch (NumberFormatException e) {
                                        latitude1 = 0.00;
                                    }

                                    double latitude2;
                                    double longitude2;

                                    try {
                                        latitude2 = Double.parseDouble(App.getData("latitude"));
                                    } catch (NumberFormatException e) {
                                        latitude2 = 0.00;
                                    }

                                    try {
                                        longitude2 = Double.parseDouble(App.getData("longitude"));
                                    } catch (NumberFormatException e) {
                                        longitude2 = 0.00;
                                    }

                                    double jarak = App.distance(new LatLng(latitude1,longitude1), new LatLng(latitude2, longitude2));

                                    Unit a = new Unit(id_unit, nama_unit, alamat, telp, gambar,
                                            url, deskripsi, latitude, longitude, id_login, id_kecamatan, nama_kecamatan, username, password);
                                    a.setJarak(jarak);
                                    unitList.add(a);
                                }
                                Collections.sort(unitList, new Comparator<Unit>() {
                                    @Override
                                    public int compare(Unit o1, Unit o2) {
                                        return Double.compare(o1.getJarak(), o2.getJarak());
                                    }
                                });
                                adapter.notifyDataSetChanged();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                load.hidepDialog();
                swipe.setRefreshing(false);
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("ERRORUNIT", error.toString());
                nodata.setText(getString(R.string.noresponse));
                nodata.setVisibility(View.VISIBLE);
                load.hidepDialog();
                swipe.setRefreshing(false);
            }
        });
        App.getInstance().addToRequestQueue(jsonReq);
    }

    private void HapusUnit(final String id_pendaftar, final String id_login){
        load.showpDialog();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, HAPUSUNIT,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("ResponseHapusUNIt", response);
                        if (response != null) {
                            try {
                                JSONObject Jsondata = new JSONObject(response);
                                String status = Jsondata.getString("status");
                                if (status.equals("0")) {
                                    Toast.makeText(TerdekatActivity.this, "Maaf, gagal menghapus" , Toast.LENGTH_SHORT);
                                } else {
                                    Toast.makeText(TerdekatActivity.this, "Berhasil menghapus data :)" , Toast.LENGTH_SHORT);
                                    getUnit();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        load.hidepDialog();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("ERRORHAPUSUNIT", error.toString());
                        load.hidepDialog();
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("id_unit", id_pendaftar);
                params.put("id_login", id_login);
                return params;
            }
        };

        App.getInstance().addToRequestQueue(stringRequest);
    }

    private class UnitAdapter extends RecyclerView.Adapter<UnitAdapter.MyViewHolder> implements Filterable {

        private List<Unit> unitList;
        ArrayList<Unit> filterlist;
        UnitFilter filter;

        public class MyViewHolder extends RecyclerView.ViewHolder {

            private TextView nama_unit;
            private TextView alamat;
            private TextView telp;
            private TextView url;
            private TextView jarak;
            private TextView waktutempuh;
           /* private TextView deskripsi;
            private TextView latitude;
            private TextView longitude;
            private TextView nama_kecamatan;
            private TextView username;
            private TextView password;*/

            private LinearLayout linear;
            private ImageView gambar;

            public MyViewHolder(View view) {
                super(view);

                nama_unit = (TextView) view.findViewById(R.id.nama_unit);
                alamat = (TextView) view.findViewById(R.id.alamat);
                telp = (TextView) view.findViewById(R.id.telp);
                url = (TextView) view.findViewById(R.id.url);
                jarak = (TextView) view.findViewById(R.id.jarak);
                waktutempuh = (TextView) view.findViewById(R.id.waktutempuh);
                /*deskripsi = (TextView) view.findViewById(R.id.deskripsi);
                latitude = (TextView) view.findViewById(R.id.latitude);
                longitude = (TextView) view.findViewById(R.id.longitude);
                nama_kecamatan = (TextView) view.findViewById(R.id.nama_kecamatan);
                username = (TextView) view.findViewById(R.id.username);
                password = (TextView) view.findViewById(R.id.password);*/

                gambar = (ImageView) view.findViewById(R.id.gambar);

                linear = (LinearLayout) view.findViewById(R.id.linear);
            }
        }

        public UnitAdapter(ArrayList<Unit> unitList) {
            this.unitList = unitList;
            this.filterlist = unitList;
        }

        @Override
        public UnitAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.terdekat_layout, parent, false);
            return new UnitAdapter.MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(final UnitAdapter.MyViewHolder holder, final int position) {
            final Unit unit = unitList.get(position);

            holder.nama_unit.setText(unit.getNama_unit());
            holder.alamat.setText(unit.getAlamat());
            holder.telp.setText(unit.getTelp());
            holder.url.setText(unit.getUrl());

            String posisiku = App.getData("latitude")+","+App.getData("longitude");
            String posisitujuan = unit.getLatitude()+","+unit.getLongitude();

            getAPIgoogle(holder.jarak, holder.waktutempuh, posisiku, posisitujuan, position);

            if(unit.getGambar() != null && !unit.getGambar().equals("")){
                Picasso.with(TerdekatActivity.this)
                        .load(URL_FOTO_UNIT+unit.getGambar())
                        .placeholder(R.drawable.ic_loading)
                        .error(R.drawable.ic_image)
                        .into(holder.gambar);
            } else {
                holder.gambar.setImageResource(R.drawable.ic_image);
            }

            holder.gambar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(TerdekatActivity.this, ImagePreviewActivity.class);
                    intent.putExtra("url", URL_FOTO_UNIT+unit.getGambar());
                    startActivity(intent);
                }
            });

            holder.linear.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(TerdekatActivity.this, DetailUnitActivity.class);
                    intent.putExtra("id_unit", unit.getId_unit());
                    intent.putExtra("nama_unit", unit.getNama_unit());
                    intent.putExtra("alamat", unit.getAlamat());
                    intent.putExtra("telp", unit.getTelp());
                    intent.putExtra("gambar", unit.getGambar());
                    intent.putExtra("url", unit.getUrl());
                    intent.putExtra("deskripsi", unit.getDeskripsi());
                    intent.putExtra("latitude", unit.getLatitude());
                    intent.putExtra("longitude", unit.getLongitude());
                    intent.putExtra("id_login", unit.getId_login());
                    intent.putExtra("username", unit.getUsername());
                    intent.putExtra("password", unit.getPassword());
                    intent.putExtra("nama_kecamatan", unit.getNama_kecamatan());
                    intent.putExtra("from", "pendaftar");
                    startActivity(intent);
                }
            });
        }

        @Override
        public int getItemCount() {
            return unitList.size();
        }

        @Override
        public Filter getFilter() {
            if(filter == null){
                filter = new UnitFilter(filterlist, this);
            }
            return filter;
        }
    }

    private void getAPIgoogle(final TextView jarak, final TextView waktutempuh, final String origin, final String destination, final int position) {
        //kload.showpDialog();
        JsonObjectRequest jsonReq = new JsonObjectRequest(Request.Method.GET, Config.GET_JARAK_API_GOOGLE+"&origin="+origin+"&destination="+destination, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("ResponseAPI", response.toString());
                        if (response != null) {
                            try {
                                String status = response.getString("status");
                                if (status.equals("OK")) {
                                    JSONArray route = response.getJSONArray("routes");
                                    JSONObject dataroute = (JSONObject) route.get(0);
                                    JSONObject overviewPolylines = dataroute.getJSONObject("overview_polyline");
                                    String encodedString = overviewPolylines.getString("points");
                                    //busList.get(position).setLine(encodedString);

                                    JSONArray legs = dataroute.getJSONArray("legs");
                                    JSONObject datalegs = (JSONObject) legs.get(0);

                                    JSONObject distance = datalegs.getJSONObject("distance");
                                    String textdistance = distance.has("text") ? distance.getString("text") : "";
                                    String valuedistance = distance.has("value") ? distance.getString("value") : "";

                                    JSONObject duration = datalegs.getJSONObject("duration");
                                    String textduration = duration.has("text") ? duration.getString("text") : "";
                                    String valueduration = duration.has("value") ? duration.getString("value") : "";

                                    jarak.setText(textdistance);
                                    waktutempuh.setText(textduration);

                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        //load.hidepDialog();
                        //swipe.setRefreshing(false);
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("errorApi", error.toString());
                //  load.hidepDialog();
            }
        });
        App.getInstance().addToRequestQueue(jsonReq);
    }

    private class UnitFilter extends Filter {

        private UnitAdapter adapter;
        private ArrayList<Unit> filterList;

        public UnitFilter(ArrayList<Unit> filterList, UnitAdapter adapter){
            this.filterList = filterList;
            this.adapter = adapter;
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();
            //check constraint
            if(constraint != null && constraint.length() > 0){
                ArrayList<Unit> filteredUnit = new ArrayList<>();

                for (int i = 0; i < filterList.size(); i++){
                    if (filterList.get(i).getNama_unit().toUpperCase().contains(constraint.toString().toUpperCase())
                            | filterList.get(i).getNama_unit().toUpperCase().contains(constraint.toString().toUpperCase())
                            ){
                        filteredUnit.add(filterList.get(i));
                    }
                }
                results.count = filteredUnit.size();
                results.values = filteredUnit;
            } else {
                results.count = filterList.size();
                results.values = filterList;
            }
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            adapter.unitList = (ArrayList<Unit>) results.values;
            if(results.count > 0){
                nodata.setVisibility(View.GONE);
            } else {
                nodata.setText("Data tidak ditemukan");
                nodata.setVisibility(View.VISIBLE);
            }
            adapter.notifyDataSetChanged();

        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }
}
