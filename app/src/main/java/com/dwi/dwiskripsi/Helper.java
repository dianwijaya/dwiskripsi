package com.dwi.dwiskripsi;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class Helper {

    public static String convertDate(String date, String informat, String outformat){
        Date recived = null;
        SimpleDateFormat formatrecived = new SimpleDateFormat(informat, Locale.US);
        SimpleDateFormat formatwanted = new SimpleDateFormat(outformat, Locale.US);

        try {
            recived = formatrecived.parse(date);
        } catch (ParseException e) {
            Log.e("GAGALPARSEDATE", e.toString());
        }
        return formatwanted.format(recived);
    }

    public static String formatNumber(String number){
        NumberFormat formatter = new DecimalFormat("#,###");
        double myNumber = Double.parseDouble(number);
        String hasilformat = formatter.format(myNumber);

        return "IDR "+hasilformat.replace(",",".");
    }

    public static void cekPermission(Activity activity){
        int MyVersion = Build.VERSION.SDK_INT;
        if (MyVersion > Build.VERSION_CODES.LOLLIPOP_MR1) {

            int result1 = ContextCompat.checkSelfPermission(activity, android.Manifest.permission.READ_EXTERNAL_STORAGE);
            int result2 = ContextCompat.checkSelfPermission(activity, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
            int result3 = ContextCompat.checkSelfPermission(activity, android.Manifest.permission.CAMERA);
            int result4 = ContextCompat.checkSelfPermission(activity, android.Manifest.permission.ACCESS_FINE_LOCATION);
            int result5 = ContextCompat.checkSelfPermission(activity, android.Manifest.permission.ACCESS_COARSE_LOCATION);

            if (result1 == PackageManager.PERMISSION_DENIED &&
                    result2 == PackageManager.PERMISSION_DENIED &&
                    result3 == PackageManager.PERMISSION_DENIED &&
                    result4 == PackageManager.PERMISSION_DENIED &&
                    result5 == PackageManager.PERMISSION_DENIED) {

                ActivityCompat.requestPermissions(activity, new String[]{
                        android.Manifest.permission.READ_EXTERNAL_STORAGE,
                        android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        android.Manifest.permission.CAMERA,
                        android.Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.ACCESS_COARSE_LOCATION}, 101);
            }
        }
    }
}
