package com.dwi.dwiskripsi.admin.unit;

import android.location.Address;
import android.location.Geocoder;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.dwi.dwiskripsi.App;
import com.dwi.dwiskripsi.Loading;
import com.dwi.dwiskripsi.R;
import com.dwi.dwiskripsi.unit.EditProfileUnitActivity;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.List;

import static com.dwi.dwiskripsi.Config.LATITUDE;
import static com.dwi.dwiskripsi.Config.LONGITUDE;

public class MapsActivity extends AppCompatActivity {

    private MapView mapView;
    private GoogleMap googleMap;
    private Button buttonLihat;
    private ImageButton btnsearch;
    private Marker marker;
    private EditText search;
    private Loading load;
    private String lokasilatitude, lokasilongitude;
    private String from;
    private String longitude, latitude;
    private double lati, longi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("Maps");

        from = getIntent().getExtras().getString("from");
        latitude = getIntent().getExtras().getString("latitude");
        longitude = getIntent().getExtras().getString("longitude");

        if(latitude.equals("") | longitude.equals("")){
            lati = LATITUDE;
            longi = LONGITUDE;
        } else {
            try {
                lati = Double.parseDouble(latitude);
            } catch (NumberFormatException e) {
                lati = 0.000;
            }

            try {
                longi = Double.parseDouble(longitude);
            } catch (NumberFormatException e) {
                longi = 0.000;
            }
        }

        load = new Loading(this);
        lokasilatitude = "";
        lokasilongitude = "";

        search = (EditText) findViewById(R.id.search);
        buttonLihat = (Button) findViewById(R.id.buttonLihat);
        btnsearch = (ImageButton) findViewById(R.id.btnsearch);
        btnsearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Geocoder coder = new Geocoder(MapsActivity.this);
                List<Address> address;
                try {
                    address = coder.getFromLocationName(search.getText().toString(), 5);
                    if (address.size() > 0) {
                        Address location = address.get(0);
                        location.getLatitude();
                        location.getLongitude();

                        marker.setPosition(new LatLng(location.getLatitude(), location.getLongitude()));

                        CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(location.getLatitude(), location.getLongitude())).zoom(16).build();
                        googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

                        lokasilatitude = String.valueOf(location.getLatitude());
                        lokasilongitude = String.valueOf(location.getLongitude());
                    } else {
                        Toast.makeText(MapsActivity.this, "Tempat tidak ditemukan", Toast.LENGTH_SHORT).show();
                    }
                } catch (IOException ex) {
                    ex.printStackTrace();
                    Log.e("ERRORGEO", ex.toString());
                }
            }
        });

        mapView = (MapView) findViewById(R.id.map);
        mapView.onCreate(savedInstanceState);
        mapView.onResume();

        try {
            MapsInitializer.initialize(this);
        } catch (Exception e) {
            e.printStackTrace();
        }

        mapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap mMap) {
                CameraPosition cameraPosition;
                googleMap = mMap;

                marker = googleMap.addMarker(new MarkerOptions().position(new LatLng(lati, longi))
                        .title("Lokasi")
                        .draggable(true));

                cameraPosition = new CameraPosition.Builder().target(new LatLng(lati, longi)).zoom(16).build();

                googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                googleMap.setOnMarkerDragListener(new GoogleMap.OnMarkerDragListener() {

                    @Override
                    public void onMarkerDrag(Marker arg0) {
                        Log.e("Marker", "Dragging");
                    }

                    @Override
                    public void onMarkerDragEnd(Marker marker) {
                        LatLng markerLocation = marker.getPosition();

                        lokasilatitude = String.valueOf(markerLocation.latitude);
                        lokasilongitude = String.valueOf(markerLocation.longitude);

                        Log.e("Marker", "finished");
                        Log.e("MarkerPOSITION", lokasilatitude+","+lokasilongitude);
                    }

                    @Override
                    public void onMarkerDragStart(Marker arg0) {
                        Log.e("Marker", "Started");

                    }
                });

                lokasilatitude = String.valueOf(marker.getPosition().latitude);
                lokasilongitude = String.valueOf(marker.getPosition().longitude);
            }
        });

        buttonLihat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(from.equalsIgnoreCase("tambah")){
                    TambahUnitActivity.Elatitude.setText(String.valueOf(lokasilatitude));
                    TambahUnitActivity.Elongitude.setText(String.valueOf(lokasilongitude));
                    finish();
                } else if(from.equalsIgnoreCase("edit")){
                    EditUnitActivity.Elatitude.setText(String.valueOf(lokasilatitude));
                    EditUnitActivity.Elongitude.setText(String.valueOf(lokasilongitude));
                    finish();
                } else if(from.equalsIgnoreCase("editprofilunit")){
                    EditProfileUnitActivity.Elatitude.setText(String.valueOf(lokasilatitude));
                    EditProfileUnitActivity.Elongitude.setText(String.valueOf(lokasilongitude));
                    finish();
                }
            }
        });

        App.cekPermission(this);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                onBackPressed();
        }
        return (super.onOptionsItemSelected(menuItem));
    }

    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

}
