package com.dwi.dwiskripsi.admin.admin;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.dwi.dwiskripsi.App;
import com.dwi.dwiskripsi.ImagePreviewActivity;
import com.dwi.dwiskripsi.admin.unit.DataUnitActivity;
import com.dwi.dwiskripsi.model.Admin;
import com.dwi.dwiskripsi.Loading;
import com.dwi.dwiskripsi.R;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.dwi.dwiskripsi.Config.GETADMIN;
import static com.dwi.dwiskripsi.Config.HAPUSADMIN;
import static com.dwi.dwiskripsi.Config.ID_LOGIN;
import static com.dwi.dwiskripsi.Config.URL_FOTO_ADMIN;
import static com.dwi.dwiskripsi.Config.URL_FOTO_UNIT;

public class DataAdminActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private AdminAdapter adapter;
    private ArrayList<Admin> adminList = new ArrayList<>();
    private SwipeRefreshLayout swipe;
    private Loading load;
    private TextView nodata;
    private SearchView sv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_admin);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("Data Admin Pusat");

        sv = (SearchView) findViewById(R.id.seacrh);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        nodata = (TextView) findViewById(R.id.nodata);
        swipe = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh);
        load = new Loading(this);

        adapter = new AdminAdapter(adminList);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);

        sv.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                adapter.getFilter().filter(newText);
                return false;
            }
        });

        swipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getAdmin();
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        getAdmin();
    }

    private void getAdmin() {
        load.showpDialog();
        JsonObjectRequest jsonReq = new JsonObjectRequest(Request.Method.GET, GETADMIN, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("ResponseKecamatan", response.toString());
                if (response != null) {
                    try {
                        adminList.clear();
                        String status = response.getString("status");
                        if (status.equals("0")) {
                            nodata.setVisibility(View.VISIBLE);
                            nodata.setText(getString(R.string.nodata));
                        } else {
                            nodata.setVisibility(View.GONE);
                            try {
                                JSONArray feedArray = response.getJSONArray("data");

                                for (int i = 0; i < feedArray.length(); i++) {
                                    JSONObject data = (JSONObject) feedArray.get(i);

                                    String id_admin = data.has("id_admin") ? data.getString("id_admin") : "";
                                    String nama = data.has("nama") ? data.getString("nama") : "";
                                    String alamat = data.has("alamat") ? data.getString("alamat") : "";
                                    String id_login = data.has("id_login") ? data.getString("id_login") : "";
                                    String username = data.has("username") ? data.getString("username") : "";
                                    String password = data.has("password") ? data.getString("password") : "";
                                    String gambar = data.has("gambar") ? data.getString("gambar") : "";

                                    Admin a = new Admin(id_admin, nama, alamat, id_login,username,password,gambar);
                                    adminList.add(a);

                                }
                                adapter.notifyDataSetChanged();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                load.hidepDialog();
                swipe.setRefreshing(false);
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("ERRORKECAMATAN", error.toString());
                nodata.setText(getString(R.string.noresponse));
                nodata.setVisibility(View.VISIBLE);
                load.hidepDialog();
                swipe.setRefreshing(false);
            }
        });
        App.getInstance().addToRequestQueue(jsonReq);
    }

    private void HapusAdmin(final String id_admin, final String id_login){
        load.showpDialog();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, HAPUSADMIN,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("ResponseHapusadmin", response);
                        if (response != null) {
                            try {
                                JSONObject Jsondata = new JSONObject(response);
                                String status = Jsondata.getString("status");
                                if (status.equals("0")) {
                                    Toast.makeText(DataAdminActivity.this, "Maaf, gagal menghapus" , Toast.LENGTH_SHORT);
                                } else {
                                    Toast.makeText(DataAdminActivity.this, "Berhasil menghapus data :)" , Toast.LENGTH_SHORT);
                                    getAdmin();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        load.hidepDialog();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("ERRORHAPUSadmin", error.toString());
                        load.hidepDialog();
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("id_admin", id_admin);
                params.put("id_login", id_login);
                return params;
            }
        };

        App.getInstance().addToRequestQueue(stringRequest);
    }

    private class AdminAdapter extends RecyclerView.Adapter<AdminAdapter.MyViewHolder> implements Filterable {

        private List<Admin> adminList;
        ArrayList<Admin> filterlist;
        AdminFilter filter;

        public class MyViewHolder extends RecyclerView.ViewHolder {
            private TextView nama, alamat;
            private LinearLayout linear;
            private CircleImageView foto;

            public MyViewHolder(View view) {
                super(view);
                nama = (TextView) view.findViewById(R.id.nama);
                alamat = (TextView) view.findViewById(R.id.alamat);
                linear = (LinearLayout) view.findViewById(R.id.linear);
                foto = (CircleImageView) view.findViewById(R.id.gambar);
            }
        }

        public AdminAdapter(ArrayList<Admin> kecamatanList) {
            this.adminList = kecamatanList;
            this.filterlist = kecamatanList;
        }

        @Override
        public AdminAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.data_admin_layout, parent, false);
            return new AdminAdapter.MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(final AdminAdapter.MyViewHolder holder, final int position) {
            final Admin admin = adminList.get(position);

            holder.nama.setText(admin.getNama());
            holder.alamat.setText(admin.getAlamat());

            if(admin.getGambar() != null && !admin.getGambar().equals("")){
                Picasso.with(DataAdminActivity.this)
                        .load(URL_FOTO_ADMIN+admin.getGambar())
                        .placeholder(R.drawable.ic_loading)
                        .error(R.drawable.ic_image)
                        .into(holder.foto);
            } else {
                holder.foto.setImageResource(R.drawable.ic_image);
            }

            holder.foto.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(DataAdminActivity.this, ImagePreviewActivity.class);
                    intent.putExtra("url", URL_FOTO_ADMIN+admin.getGambar());
                    startActivity(intent);
                }
            });

            holder.linear.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(DataAdminActivity.this);

                    LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    View dialogView = inflater.inflate(R.layout.dialog_edit_delete_layout, null);
                    dialogBuilder.setView(dialogView);

                    Button edit = (Button) dialogView.findViewById(R.id.edit);
                    Button hapus = (Button) dialogView.findViewById(R.id.hapus);

                    if(App.getData(ID_LOGIN).equals(admin.getId_login())){
                        hapus.setVisibility(View.GONE);
                    } else {
                        hapus.setVisibility(View.VISIBLE);
                    }

                    final AlertDialog alertDialog = dialogBuilder.create();
                    alertDialog.setCancelable(true);

                    edit.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            alertDialog.dismiss();
                            Intent intent = new Intent(DataAdminActivity.this, EditAdminActivity.class);
                            intent.putExtra("id_admin", admin.getId_admin());
                            intent.putExtra("nama", admin.getNama());
                            intent.putExtra("alamat", admin.getAlamat());
                            intent.putExtra("username", admin.getUsername());
                            intent.putExtra("password", admin.getPassword());
                            intent.putExtra("id_login", admin.getId_login());
                            intent.putExtra("gambar", admin.getGambar());
                            startActivity(intent);
                        }
                    });

                    hapus.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            alertDialog.dismiss();
                            AlertDialog.Builder builder1 = new AlertDialog.Builder(DataAdminActivity.this);
                            builder1.setMessage("Apakah Anda yakin ingin menghapus ini ?");
                            builder1.setCancelable(false);

                            builder1.setPositiveButton("Ya",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            HapusAdmin(admin.getId_admin(), admin.getId_login());
                                            dialog.cancel();
                                        }
                                    });

                            builder1.setNegativeButton("Tidak",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.cancel();
                                        }
                                    });

                            AlertDialog alert11 = builder1.create();
                            alert11.show();
                        }
                    });


                    if (!alertDialog.isShowing()) {
                        alertDialog.show();
                    }

                    return false;
                }
            });

        }

        @Override
        public int getItemCount() {
            return adminList.size();
        }

        @Override
        public Filter getFilter() {
            if(filter == null){
                filter = new AdminFilter(filterlist, this);
            }
            return filter;
        }
    }

    private class AdminFilter extends Filter {

        private AdminAdapter adapter;
        private ArrayList<Admin> filterList;

        public AdminFilter(ArrayList<Admin> filterList, AdminAdapter adapter){
            this.filterList = filterList;
            this.adapter = adapter;
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();
            //check constraint
            if(constraint != null && constraint.length() > 0){
                ArrayList<Admin> filteredAdmin = new ArrayList<>();

                for (int i = 0; i < filterList.size(); i++){
                    if (filterList.get(i).getNama().toUpperCase().contains(constraint.toString().toUpperCase())
                            | filterList.get(i).getNama().toUpperCase().contains(constraint.toString().toUpperCase())
                            ){
                        filteredAdmin.add(filterList.get(i));
                    }
                }
                results.count = filteredAdmin.size();
                results.values = filteredAdmin;
            } else {
                results.count = filterList.size();
                results.values = filterList;
            }
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            adapter.adminList = (ArrayList<Admin>) results.values;
            if(results.count > 0){
                nodata.setVisibility(View.GONE);
            } else {
                nodata.setText("Data tidak ditemukan");
                nodata.setVisibility(View.VISIBLE);
            }
            adapter.notifyDataSetChanged();

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.tambah, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.tambah) {
            Intent intent = new Intent(DataAdminActivity.this, TambahAdminActivity.class);
            startActivity(intent);
            return true;
        } else if (id == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }
}

