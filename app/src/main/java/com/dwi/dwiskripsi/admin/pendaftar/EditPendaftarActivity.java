package com.dwi.dwiskripsi.admin.pendaftar;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.dwi.dwiskripsi.App;
import com.dwi.dwiskripsi.Helper;
import com.dwi.dwiskripsi.Loading;
import com.dwi.dwiskripsi.R;
import com.dwi.dwiskripsi.model.Unit;
import com.soundcloud.android.crop.Crop;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static com.dwi.dwiskripsi.Config.GETUNIT;
import static com.dwi.dwiskripsi.Config.UPDATEPENDAFTAR;
import static com.dwi.dwiskripsi.Config.URL_FOTO_PENDAFTAR;

public class EditPendaftarActivity extends AppCompatActivity {

    private Button tambah;
    private Loading load;

    private String id_pendaftar;
    private String nama_lengkap;
    private String nama_pendek;
    private String tempat_lahir;
    private String tanggal_lahir;
    private String agama;
    private String nama_ortu;
    private String alamat;
    private String telp;
    private String nama_sekolah;
    private String kelas;
    private String username;
    private String password;
    private String id_login;
    private String status;
    private String id_unit;
    private String gambar;

    private EditText Enama_lengkap, Enama_pendek, Etempat_lahir, Etanggal_lahir,
            Enama_ortu, Ealamat, Etelp, Enama_sekolah, Ekelas, Eusername, Epassword;
    private Spinner Sagama, Sunit, Sstatus;
    private ArrayList<Unit> unitList = new ArrayList<>();
    private Calendar myCalendar;

    //photo
    private Uri imageUriCamera;
    private int REQUEST_CAMERA = 3;
    private ImageView ViewGambar;
    private String hasilencodegambar = "";
    private Button pilihgambar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_pendaftar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("Edit Siswa");

        load = new Loading(this);

        id_pendaftar = getIntent().getExtras().getString("id_pendaftar");
        nama_lengkap = getIntent().getExtras().getString("nama_lengkap");
        nama_pendek = getIntent().getExtras().getString("nama_pendek");
        tempat_lahir = getIntent().getExtras().getString("tempat_lahir");
        tanggal_lahir = getIntent().getExtras().getString("tanggal_lahir");
        agama = getIntent().getExtras().getString("agama");
        nama_ortu = getIntent().getExtras().getString("nama_ortu");
        alamat = getIntent().getExtras().getString("alamat");
        telp = getIntent().getExtras().getString("telp");
        nama_sekolah = getIntent().getExtras().getString("nama_sekolah");
        kelas = getIntent().getExtras().getString("kelas");
        username = getIntent().getExtras().getString("username");
        password = getIntent().getExtras().getString("password");
        id_login = getIntent().getExtras().getString("id_login");
        status = getIntent().getExtras().getString("status");
        id_unit = getIntent().getExtras().getString("id_unit");
        gambar = getIntent().getExtras().getString("gambar");

        tambah = (Button) findViewById(R.id.tambah);

        Enama_lengkap = (EditText) findViewById(R.id.nama_lengkap);
        Enama_pendek = (EditText) findViewById(R.id.nama_pendek);
        Etempat_lahir = (EditText) findViewById(R.id.tempat_lahir);
        Etanggal_lahir = (EditText) findViewById(R.id.tanggal_lahir);
        Enama_ortu = (EditText) findViewById(R.id.nama_ortu);
        Ealamat = (EditText) findViewById(R.id.alamat);
        Etelp = (EditText) findViewById(R.id.telp);
        Enama_sekolah = (EditText) findViewById(R.id.nama_sekolah);
        Ekelas = (EditText) findViewById(R.id.kelas);
        Eusername = (EditText) findViewById(R.id.username);
        Epassword = (EditText) findViewById(R.id.password);
        Sagama = (Spinner) findViewById(R.id.agama);
        Sunit = (Spinner) findViewById(R.id.unit);
        Sstatus = (Spinner) findViewById(R.id.status);

        ViewGambar = (ImageView) findViewById(R.id.gambar);
        if(gambar != null && !gambar.equals("")){
            Picasso.with(EditPendaftarActivity.this)
                    .load(URL_FOTO_PENDAFTAR+gambar)
                    .placeholder(R.drawable.ic_loading)
                    .error(R.drawable.ic_image)
                    .into(ViewGambar);
        } else {
            ViewGambar.setImageResource(R.drawable.ic_image);
        }
        pilihgambar = (Button) findViewById(R.id.pilihgambar);
        pilihgambar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImage();
            }
        });


        populatingUnit();

        ArrayAdapter<String> agamadata = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, getResources().getStringArray(R.array.agama));
        Sagama.setAdapter(agamadata);
        if (agama != null) {
            int spinnerPosition = agamadata.getPosition(agama);
            Sagama.setSelection(spinnerPosition);
        }

        ArrayAdapter<String> statusdata = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, getResources().getStringArray(R.array.status));
        Sstatus.setAdapter(statusdata);
        if (status != null) {
            int spinnerPositio = statusdata.getPosition(status);
            Sstatus.setSelection(spinnerPositio);
        }

        Enama_lengkap.setText(nama_lengkap);
        Enama_pendek.setText(nama_pendek);
        Etempat_lahir.setText(tempat_lahir);
        Etanggal_lahir.setText(Helper.convertDate(tanggal_lahir,"yyyy-MM-dd","dd MMMM yyyy"));
        Enama_ortu.setText(nama_ortu);
        Ealamat.setText(alamat);
        Etelp.setText(telp);
        Enama_sekolah.setText(nama_sekolah);
        Ekelas.setText(kelas);
        Eusername.setText(username);
        Epassword.setText(password);

        tambah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Enama_lengkap.getText().toString().isEmpty()){
                    Enama_lengkap.setError("Harus diisi.");
                } else if(Enama_pendek.getText().toString().isEmpty()){
                    Enama_pendek.setError("Harus diisi.");
                } else if(Etempat_lahir.getText().toString().isEmpty()){
                    Etempat_lahir.setError("Harus diisi.");
                } else if(Etanggal_lahir.getText().toString().isEmpty()){
                    Etanggal_lahir.setError("Harus diisi.");
                } else if(Enama_ortu.getText().toString().isEmpty()){
                    Enama_ortu.setError("Harus diisi.");
                } else if(Ealamat.getText().toString().isEmpty()){
                    Ealamat.setError("Harus diisi.");
                } else if(Etelp.getText().toString().isEmpty()){
                    Etelp.setError("Harus diisi.");
                } else if(Enama_sekolah.getText().toString().isEmpty()){
                    Enama_sekolah.setError("Harus diisi.");
                } else if(Ekelas.getText().toString().isEmpty()){
                    Ekelas.setError("Harus diisi.");
                } else if(Eusername.getText().toString().isEmpty()){
                    Eusername.setError("Harus diisi.");
                } else if(Epassword.getText().toString().isEmpty()){
                    Epassword.setError("Harus diisi.");
                } else if(id_unit.equals("")) {
                    Toast.makeText(EditPendaftarActivity.this, "Anda belum memilih unit.", Toast.LENGTH_SHORT).show();
                } else {
                    EditPendaftar();
                }
            }
        });


        myCalendar = Calendar.getInstance();
        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                String myFormat = "dd MMMM yyyy";
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                Etanggal_lahir.setText(sdf.format(myCalendar.getTime()));
            }
        };

        Etanggal_lahir.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                new DatePickerDialog(EditPendaftarActivity.this, date, myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
    }

    private void populatingUnit(){
        load.showpDialog();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, GETUNIT,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("ResponseUnit", response);
                        if (response != null) {
                            try {
                                JSONObject Jsondata = new JSONObject(response);
                                JSONArray feedArray = Jsondata.getJSONArray("data");

                                for (int i = 0; i < feedArray.length(); i++) {
                                    JSONObject data = (JSONObject) feedArray.get(i);

                                    String id_unit = data.has("id_unit") ? data.getString("id_unit") : "";
                                    String nama_unit = data.has("nama_unit") ? data.getString("nama_unit") : "";
                                    String alamat = data.has("alamat") ? data.getString("alamat") : "";
                                    String telp = data.has("telp") ? data.getString("telp") : "";
                                    String gambar = data.has("gambar") ? data.getString("gambar") : "";
                                    String url = data.has("url") ? data.getString("url") : "";
                                    String deskripsi = data.has("deskripsi") ? data.getString("deskripsi") : "";
                                    String latitude = data.has("latitude") ? data.getString("latitude") : "";
                                    String longitude = data.has("longitude") ? data.getString("longitude") : "";
                                    String id_login = data.has("id_login") ? data.getString("id_login") : "";
                                    String id_kecamatan = data.has("id_kecamatan") ? data.getString("id_kecamatan") : "";
                                    String nama_kecamatan = data.has("nama_kecamatan") ? data.getString("nama_kecamatan") : "";
                                    String username = data.has("username") ? data.getString("username") : "";
                                    String password = data.has("password") ? data.getString("password") : "";


                                    Unit a = new Unit(id_unit, nama_unit, alamat, telp, gambar,
                                            url, deskripsi, latitude, longitude, id_login, id_kecamatan, nama_kecamatan, username, password);
                                    unitList.add(a);

                                }

                                List<String> lables = new ArrayList<String>();
                                for (int i = 0; i < unitList.size(); i++) {
                                    lables.add(unitList.get(i).getNama_unit());
                                }

                                final ArrayAdapter<String> adapter = new ArrayAdapter<String> (EditPendaftarActivity.this, android.R.layout.simple_spinner_item, lables);
                                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                Sunit.setAdapter(adapter);
                                Sunit.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                    @Override
                                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                        id_unit = unitList.get(position).getId_unit();
                                    }

                                    @Override
                                    public void onNothingSelected(AdapterView<?> parent) {

                                    }
                                });

                                for(int i = 0; i < unitList.size(); i++){
                                    if(unitList.get(i).getId_unit().equals(id_unit)){
                                        Sunit.setSelection(i);
                                    }
                                }

                            } catch (JSONException e) {
                                Log.e("ErroJSon", e.toString());
                            }
                        }
                        load.hidepDialog();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("ERRORUNIT", error.toString());
                        load.hidepDialog();
                    }
                });
        App.getInstance().addToRequestQueue(stringRequest);
    }

    private void EditPendaftar(){
        load.showpDialog();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, UPDATEPENDAFTAR,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("ResponseEditKecamatan", response);
                        if (response != null) {
                            try {
                                JSONObject Jsondata = new JSONObject(response);
                                String status = Jsondata.getString("status");
                                if (status.equals("0")) {
                                    Toast.makeText(EditPendaftarActivity.this, "Maaf, gagal menambahkan data :(" , Toast.LENGTH_SHORT);
                                } else {
                                    Toast.makeText(EditPendaftarActivity.this, "Berhasil menambahkan data :)" , Toast.LENGTH_SHORT);
                                    finish();
                                }
                            } catch (JSONException e) {
                                Log.e("ErroJSon", e.toString());
                            }
                        }
                        load.hidepDialog();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("ERROREDITKECAMATAN", error.toString());
                        load.hidepDialog();
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("id_pendaftar", id_pendaftar);
                params.put("nama_lengkap", Enama_lengkap.getText().toString());
                params.put("nama_pendek", Enama_pendek.getText().toString());
                params.put("tempat_lahir", Etempat_lahir.getText().toString());
                params.put("tanggal_lahir", Helper.convertDate(Etanggal_lahir.getText().toString(), "dd MMMM yyyy", "yyyy-MM-dd"));
                params.put("agama", Sagama.getSelectedItem().toString());
                params.put("nama_ortu", Enama_ortu.getText().toString());
                params.put("alamat", Ealamat.getText().toString());
                params.put("telp", Etelp.getText().toString());
                params.put("nama_sekolah", Enama_sekolah.getText().toString());
                params.put("kelas", Ekelas.getText().toString());
                params.put("username", Eusername.getText().toString());
                params.put("password", Epassword.getText().toString());
                params.put("id_login", id_login);
                params.put("id_unit", id_unit);
                params.put("status", Sstatus.getSelectedItem().toString());
                params.put("gambar", hasilencodegambar);
                return params;
            }
        };

        App.getInstance().addToRequestQueue(stringRequest);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                onBackPressed();
        }
        return (super.onOptionsItemSelected(menuItem));
    }

    private void selectImage() {

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(EditPendaftarActivity.this);

        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View dialogView = inflater.inflate(R.layout.dialog_upload_foto, null);
        dialogBuilder.setView(dialogView);

        Button kamera = (Button) dialogView.findViewById(R.id.kamera);
        Button gallery = (Button) dialogView.findViewById(R.id.galery);

        final AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.setCancelable(true);

        kamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
                if(imageUriCamera != null){
                    imageUriCamera = null;
                }
                takePhoto();
            }
        });

        gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
                Crop.pickImage(EditPendaftarActivity.this);
            }
        });
        alertDialog.show();
    }

    public void takePhoto() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File photo = new File(Environment.getExternalStorageDirectory(), "png.png");
        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photo));
        imageUriCamera = Uri.fromFile(photo);
        startActivityForResult(intent, REQUEST_CAMERA);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == REQUEST_CAMERA && resultCode == RESULT_OK){
            getContentResolver().notifyChange(imageUriCamera , null);
            beginCrop(imageUriCamera);
        } else if (requestCode == Crop.REQUEST_PICK && resultCode == RESULT_OK) {
            beginCrop(data.getData());
        } else if (requestCode == Crop.REQUEST_CROP) {
            handleCrop(resultCode, data);
        }
    }

    private void beginCrop(Uri source) {
        Uri destination = Uri.fromFile(new File(getCacheDir(), "cropped"));
        Crop.of(source, destination).withMaxSize(800, 800).start(this);
    }

    private void handleCrop(int resultCode, Intent result) {
        if (resultCode == RESULT_OK) {
            ViewGambar.setImageDrawable(null);
            ViewGambar.setImageURI(Crop.getOutput(result));

            try {
                Bitmap bitmapcrop = MediaStore.Images.Media.getBitmap(getContentResolver(), Crop.getOutput(result));
                hasilencodegambar = encodeImage(bitmapcrop);
                Log.e("GAMBAR", hasilencodegambar);

            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (resultCode == Crop.RESULT_ERROR) {
            Toast.makeText(this, Crop.getError(result).getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private String encodeImage(Bitmap bm) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.JPEG,90,baos);
        byte[] b = baos.toByteArray();
        String encImage = Base64.encodeToString(b, Base64.DEFAULT);
        return encImage;
    }
}
