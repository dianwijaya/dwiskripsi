package com.dwi.dwiskripsi.admin.unit;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.dwi.dwiskripsi.App;
import com.dwi.dwiskripsi.model.Kecamatan;
import com.dwi.dwiskripsi.Loading;
import com.dwi.dwiskripsi.R;
import com.soundcloud.android.crop.Crop;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.dwi.dwiskripsi.Config.GETKECAMATAN;
import static com.dwi.dwiskripsi.Config.UPDATEUNIT;
import static com.dwi.dwiskripsi.Config.URL_FOTO_UNIT;

public class EditUnitActivity extends AppCompatActivity {

    private Button tambah;

    private EditText Enama_unit;
    private EditText Ealamat;
    private EditText Etelp;
    private EditText Eurl;
    private EditText Edeskripsi;
    public static EditText Elatitude;
    public static EditText Elongitude;
    private EditText Eusername;
    private EditText Epassword;

    private String id_unit;
    private String nama_unit;
    private String alamat;
    private String telp;
    private String gambar;
    private String url;
    private String deskripsi;
    private String latitude;
    private String longitude;
    private String id_login;
    private String username;
    private String password;

    private Loading load;
    private Spinner Skecamatan;
    private ArrayList<Kecamatan> kecamatanList = new ArrayList<>();
    private String id_kecamatan = "";
    private ImageButton maps;

    //photo
    private Uri imageUriCamera;
    private int REQUEST_CAMERA = 3;
    private ImageView ViewGambar;
    private String hasilencodegambar = "";
    private Button pilihgambar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_unit);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("Edit Tempat Les");

        load = new Loading(this);



        id_unit = getIntent().getExtras().getString("id_unit");
        nama_unit = getIntent().getExtras().getString("nama_unit");
        alamat = getIntent().getExtras().getString("alamat");
        telp = getIntent().getExtras().getString("telp");
        gambar = getIntent().getExtras().getString("gambar");
        url = getIntent().getExtras().getString("url");
        deskripsi = getIntent().getExtras().getString("deskripsi");
        latitude = getIntent().getExtras().getString("latitude");
        longitude = getIntent().getExtras().getString("longitude");
        id_login = getIntent().getExtras().getString("id_login");
        username = getIntent().getExtras().getString("username");
        password = getIntent().getExtras().getString("password");
        id_kecamatan = getIntent().getExtras().getString("id_kecamatan");


        Enama_unit = (EditText) findViewById(R.id.nama_unit);
        Ealamat = (EditText) findViewById(R.id.alamat);
        Etelp = (EditText) findViewById(R.id.telp);
        Eurl = (EditText) findViewById(R.id.url);
        Edeskripsi = (EditText) findViewById(R.id.deskripsi);
        Elatitude = (EditText) findViewById(R.id.latitude);
        Elongitude = (EditText) findViewById(R.id.longitude);
        Eusername = (EditText) findViewById(R.id.username);
        Epassword = (EditText) findViewById(R.id.password);
        Skecamatan = (Spinner) findViewById(R.id.kecamatan);
        ViewGambar = (ImageView) findViewById(R.id.gambar);
        maps = (ImageButton) findViewById(R.id.maps);
        maps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(EditUnitActivity.this, MapsActivity.class);
                intent.putExtra("from", "edit");
                intent.putExtra("latitude", Elatitude.getText().toString());
                intent.putExtra("longitude", Elongitude.getText().toString());
                startActivity(intent);
            }
        });

        Enama_unit.setText(nama_unit);
        Ealamat.setText(alamat);
        Etelp.setText(telp);
        Eurl.setText(url);
        Edeskripsi.setText(deskripsi);
        Elatitude.setText(latitude);
        Elongitude.setText(longitude);
        Eusername.setText(username);
        Epassword.setText(password);

        if(gambar != null && !gambar.equals("")){
            Picasso.with(this)
                    .load(URL_FOTO_UNIT+gambar)
                    .placeholder(R.drawable.ic_loading)
                    .error(R.drawable.ic_image)
                    .into(ViewGambar);
        } else {
            ViewGambar.setImageResource(R.drawable.ic_image);
        }

        populatingKecamatan();

        pilihgambar = (Button) findViewById(R.id.pilihgambar);
        tambah = (Button) findViewById(R.id.tambah);
        tambah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Enama_unit.getText().toString().isEmpty()) {
                    Enama_unit.setError("Harus diisi.");
                } else if(Ealamat.getText().toString().isEmpty()){
                    Ealamat.setError("Harus diisi.");
                } else if(Etelp.getText().toString().isEmpty()){
                    Etelp.setError("Harus diisi.");
                } else if(Edeskripsi.getText().toString().isEmpty()){
                    Edeskripsi.setError("Harus diisi.");
                } else if(Elatitude.getText().toString().isEmpty()){
                    Elatitude.setError("Harus diisi.");
                } else if(Elongitude.getText().toString().isEmpty()){
                    Elongitude.setError("Harus diisi.");
                } else if(Eusername.getText().toString().isEmpty()){
                    Eusername.setError("Harus diisi.");
                } else if(Epassword.getText().toString().isEmpty()){
                    Epassword.setError("Harus diisi.");
                } else if(id_kecamatan.equals("")) {
                    Toast.makeText(EditUnitActivity.this, "Anda belum memilih unit.", Toast.LENGTH_SHORT).show();
                } else {
                    EditUnit();
                }
            }
        });

        pilihgambar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImage();
            }
        });
    }

    private void populatingKecamatan(){
        load.showpDialog();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, GETKECAMATAN,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("ResponseKecamatan", response);
                        if (response != null) {
                            try {
                                JSONObject Jsondata = new JSONObject(response);
                                JSONArray feedArray = Jsondata.getJSONArray("data");

                                for (int i = 0; i < feedArray.length(); i++) {
                                    JSONObject data = (JSONObject) feedArray.get(i);

                                    String id_kecamatan = data.has("id_kecamatan") ? data.getString("id_kecamatan") : "";
                                    String nama_kecamatan = data.has("nama_kecamatan") ? data.getString("nama_kecamatan") : "";

                                    Kecamatan a = new Kecamatan(id_kecamatan, nama_kecamatan);
                                    kecamatanList.add(a);


                                }

                                List<String> lables = new ArrayList<String>();
                                for (int i = 0; i < kecamatanList.size(); i++) {
                                    lables.add(kecamatanList.get(i).getNama_kecamatan());
                                }

                                final ArrayAdapter<String> adapter = new ArrayAdapter<String> (EditUnitActivity.this, android.R.layout.simple_spinner_item, lables);
                                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                Skecamatan.setAdapter(adapter);
                                Skecamatan.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                    @Override
                                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                        id_kecamatan = kecamatanList.get(position).getId_kecamatan();
                                    }

                                    @Override
                                    public void onNothingSelected(AdapterView<?> parent) {

                                    }
                                });

                                for(int i = 0; i < kecamatanList.size(); i++){
                                    if(kecamatanList.get(i).getId_kecamatan().equals(id_kecamatan)){
                                        Skecamatan.setSelection(i);
                                    }
                                }

                            } catch (JSONException e) {
                                Log.e("ErroJSon", e.toString());
                            }
                        }
                        load.hidepDialog();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("ERRORUNIT", error.toString());
                        load.hidepDialog();
                    }
                });
        App.getInstance().addToRequestQueue(stringRequest);
    }

    private void EditUnit(){
        load.showpDialog();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, UPDATEUNIT,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("ResponseKecamatan", response);
                        if (response != null) {
                            try {
                                JSONObject Jsondata = new JSONObject(response);
                                String status = Jsondata.getString("status");
                                if (status.equals("0")) {
                                    Toast.makeText(EditUnitActivity.this, "Maaf, gagal mengubah data :(" , Toast.LENGTH_SHORT);
                                } else {
                                    Toast.makeText(EditUnitActivity.this, "Berhasil mengubah data :)" , Toast.LENGTH_SHORT);
                                    finish();
                                }
                            } catch (JSONException e) {
                                Log.e("ErroJSon", e.toString());
                            }
                        }
                        load.hidepDialog();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("ERRORTAMBAHPendaftar", error.toString());
                        load.hidepDialog();
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("nama_unit", Enama_unit.getText().toString());
                params.put("alamat", Ealamat.getText().toString());
                params.put("telp", Etelp.getText().toString());
                params.put("url", Eurl.getText().toString());
                params.put("deskripsi", Edeskripsi.getText().toString());
                params.put("latitude", Elatitude.getText().toString());
                params.put("longitude", Elongitude.getText().toString());
                params.put("username", Eusername.getText().toString());
                params.put("password", Epassword.getText().toString());
                params.put("id_kecamatan", id_kecamatan);
                params.put("id_login", id_login);
                params.put("id_unit", id_unit);
                params.put("gambar", hasilencodegambar);
                return params;
            }
        };
        App.getInstance().addToRequestQueue(stringRequest);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                onBackPressed();
        }
        return (super.onOptionsItemSelected(menuItem));
    }

    private void selectImage() {

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(EditUnitActivity.this);

        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View dialogView = inflater.inflate(R.layout.dialog_upload_foto, null);
        dialogBuilder.setView(dialogView);

        Button kamera = (Button) dialogView.findViewById(R.id.kamera);
        Button gallery = (Button) dialogView.findViewById(R.id.galery);

        final AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.setCancelable(true);

        kamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
                if(imageUriCamera != null){
                    imageUriCamera = null;
                }
                takePhoto();
            }
        });

        gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
                Crop.pickImage(EditUnitActivity.this);
            }
        });
        alertDialog.show();
    }

    public void takePhoto() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File photo = new File(Environment.getExternalStorageDirectory(), "png.png");
        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photo));
        imageUriCamera = Uri.fromFile(photo);
        startActivityForResult(intent, REQUEST_CAMERA);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == REQUEST_CAMERA && resultCode == RESULT_OK){
            getContentResolver().notifyChange(imageUriCamera , null);
            beginCrop(imageUriCamera);
        } else if (requestCode == Crop.REQUEST_PICK && resultCode == RESULT_OK) {
            beginCrop(data.getData());
        } else if (requestCode == Crop.REQUEST_CROP) {
            handleCrop(resultCode, data);
        }
    }

    private void beginCrop(Uri source) {
        Uri destination = Uri.fromFile(new File(getCacheDir(), "cropped"));
        Crop.of(source, destination).withMaxSize(800, 800).start(this);
    }

    private void handleCrop(int resultCode, Intent result) {
        if (resultCode == RESULT_OK) {
            ViewGambar.setImageDrawable(null);
            ViewGambar.setImageURI(Crop.getOutput(result));

            try {
                Bitmap bitmapcrop = MediaStore.Images.Media.getBitmap(getContentResolver(), Crop.getOutput(result));
                hasilencodegambar = encodeImage(bitmapcrop);
                Log.e("GAMBAR", hasilencodegambar);

            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (resultCode == Crop.RESULT_ERROR) {
            Toast.makeText(this, Crop.getError(result).getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private String encodeImage(Bitmap bm) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.JPEG,90,baos);
        byte[] b = baos.toByteArray();
        String encImage = Base64.encodeToString(b, Base64.DEFAULT);
        return encImage;
    }
}


