package com.dwi.dwiskripsi.admin;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.dwi.dwiskripsi.App;
import com.dwi.dwiskripsi.LoginActivity;
import com.dwi.dwiskripsi.R;
import com.dwi.dwiskripsi.admin.admin.DataAdminActivity;
import com.dwi.dwiskripsi.admin.kecamatan.DataKecamatanActivity;
import com.dwi.dwiskripsi.admin.laporan.LaporanUnitActivity;
import com.dwi.dwiskripsi.admin.pendaftar.DataPendaftarActivity;
import com.dwi.dwiskripsi.admin.unit.DataUnitActivity;
import android.support.design.widget.Snackbar;

import static com.dwi.dwiskripsi.Config.USERNAME;

public class AdminActivity extends AppCompatActivity {

    private Button dataadmin, dataunit, datakecamatan, datapendaftar, laporan;
    private boolean backPressToExit = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin);
        //setTitle(App.getData(USERNAME));
        setTitle("Admin Pusat");

        dataadmin = (Button) findViewById(R.id.dataadmin);
        dataunit = (Button) findViewById(R.id.dataunit);
        datakecamatan = (Button) findViewById(R.id.datakecamatan);
        datapendaftar = (Button) findViewById(R.id.datapendaftar);
        laporan = (Button) findViewById(R.id.laporan);

        dataadmin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AdminActivity.this, DataAdminActivity.class);
                startActivity(intent);
            }
        });

        dataunit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AdminActivity.this, DataUnitActivity.class);
                startActivity(intent);
            }
        });

        datakecamatan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AdminActivity.this, DataKecamatanActivity.class);
                startActivity(intent);
            }
        });

        datapendaftar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AdminActivity.this, DataPendaftarActivity.class);
                startActivity(intent);
            }
        });

        laporan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AdminActivity.this, LaporanUnitActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.logout, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.logout) {
            App.logout();
            Intent intent = new Intent(AdminActivity.this, LoginActivity.class);
            startActivity(intent);
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {

        if (backPressToExit) {
            super.onBackPressed();
            finish();
            return;
        }
        this.backPressToExit = true;
        Snackbar.make(findViewById(R.id.myView), "Tekan sekali lagi untuk keluar.", Snackbar.LENGTH_SHORT).show();
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                backPressToExit = false;
            }
        }, 2000);
    }
}
