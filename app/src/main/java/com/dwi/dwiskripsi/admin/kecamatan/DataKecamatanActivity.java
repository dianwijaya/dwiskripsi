package com.dwi.dwiskripsi.admin.kecamatan;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.dwi.dwiskripsi.App;
import com.dwi.dwiskripsi.model.Kecamatan;
import com.dwi.dwiskripsi.Loading;
import com.dwi.dwiskripsi.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.dwi.dwiskripsi.Config.GETKECAMATAN;
import static com.dwi.dwiskripsi.Config.HAPUSKECAMATAN;

public class DataKecamatanActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private KecamatanAdapter adapter;
    private ArrayList<Kecamatan> kecamatanList = new ArrayList<>();
    private SwipeRefreshLayout swipe;
    private Loading load;
    private TextView nodata;
    private SearchView sv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_kecamatan);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("Data Kecamatan");

        sv = (SearchView) findViewById(R.id.seacrh);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        nodata = (TextView) findViewById(R.id.nodata);
        swipe = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh);
        load = new Loading(this);

        adapter = new KecamatanAdapter(kecamatanList);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);

        sv.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                adapter.getFilter().filter(newText);
                return false;
            }
        });

        swipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getKecamatan();
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        getKecamatan();
    }

    private void getKecamatan() {
        load.showpDialog();
        JsonObjectRequest jsonReq = new JsonObjectRequest(Request.Method.GET, GETKECAMATAN, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("ResponseKecamatan", response.toString());
                if (response != null) {
                    try {
                        kecamatanList.clear();
                        String status = response.getString("status");
                        if (status.equals("0")) {
                            nodata.setVisibility(View.VISIBLE);
                            nodata.setText(getString(R.string.nodata));
                        } else {
                            nodata.setVisibility(View.GONE);
                            try {
                                JSONArray feedArray = response.getJSONArray("data");

                                for (int i = 0; i < feedArray.length(); i++) {
                                    JSONObject data = (JSONObject) feedArray.get(i);

                                    String id_kecamatan = data.has("id_kecamatan") ? data.getString("id_kecamatan") : "";
                                    String nama_kecamatan = data.has("nama_kecamatan") ? data.getString("nama_kecamatan") : "";

                                    Kecamatan a = new Kecamatan(id_kecamatan, nama_kecamatan);
                                    kecamatanList.add(a);

                                }
                                adapter.notifyDataSetChanged();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                load.hidepDialog();
                swipe.setRefreshing(false);
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("ERRORKECAMATAN", error.toString());
                nodata.setText(getString(R.string.noresponse));
                nodata.setVisibility(View.VISIBLE);
                load.hidepDialog();
                swipe.setRefreshing(false);
            }
        });
        App.getInstance().addToRequestQueue(jsonReq);
    }

    private void HapusKecamatan(final String id_kecamatan){
        load.showpDialog();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, HAPUSKECAMATAN,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("ResponseHapusKecamatan", response);
                        if (response != null) {
                            try {
                                JSONObject Jsondata = new JSONObject(response);
                                String status = Jsondata.getString("status");
                                if (status.equals("0")) {
                                    Toast.makeText(DataKecamatanActivity.this, "Maaf, gagal menghapus" , Toast.LENGTH_SHORT);
                                } else {
                                    Toast.makeText(DataKecamatanActivity.this, "Berhasil menghapus data :)" , Toast.LENGTH_SHORT);
                                    getKecamatan();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        load.hidepDialog();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("ERRORHAPUSKECAMATAN", error.toString());
                        load.hidepDialog();
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("id_kecamatan", id_kecamatan);
                return params;
            }
        };

        App.getInstance().addToRequestQueue(stringRequest);
    }

    private class KecamatanAdapter extends RecyclerView.Adapter<KecamatanAdapter.MyViewHolder> implements Filterable {

        private List<Kecamatan> kecamatanList;
        ArrayList<Kecamatan> filterlist;
        KecamatanFilter filter;

        public class MyViewHolder extends RecyclerView.ViewHolder {
            private TextView nama;
            private LinearLayout linear;

            public MyViewHolder(View view) {
                super(view);
                nama = (TextView) view.findViewById(R.id.nama);
                linear = (LinearLayout) view.findViewById(R.id.linear);
            }
        }

        public KecamatanAdapter(ArrayList<Kecamatan> kecamatanList) {
            this.kecamatanList = kecamatanList;
            this.filterlist = kecamatanList;
        }

        @Override
        public KecamatanAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.data_kecamatan_layout, parent, false);
            return new KecamatanAdapter.MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(final KecamatanAdapter.MyViewHolder holder, final int position) {
            final Kecamatan kecamatan = kecamatanList.get(position);

            holder.nama.setText(kecamatan.getNama_kecamatan());
            holder.linear.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(DataKecamatanActivity.this);

                    LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    View dialogView = inflater.inflate(R.layout.dialog_edit_delete_layout, null);
                    dialogBuilder.setView(dialogView);

                    Button edit = (Button) dialogView.findViewById(R.id.edit);
                    Button hapus = (Button) dialogView.findViewById(R.id.hapus);

                    final AlertDialog alertDialog = dialogBuilder.create();
                    alertDialog.setCancelable(true);

                    edit.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            alertDialog.dismiss();
                            Intent intent = new Intent(DataKecamatanActivity.this, EditKecamatanActivity.class);
                            intent.putExtra("id_kecamatan", kecamatan.getId_kecamatan());
                            intent.putExtra("nama_kecamatan", kecamatan.getNama_kecamatan());
                            startActivity(intent);
                        }
                    });

                    hapus.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            alertDialog.dismiss();
                            AlertDialog.Builder builder1 = new AlertDialog.Builder(DataKecamatanActivity.this);
                            builder1.setMessage("Apakah Anda yakin ingin menghapus ini ?");
                            builder1.setCancelable(false);

                            builder1.setPositiveButton("Ya",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            HapusKecamatan(kecamatan.getId_kecamatan());
                                            dialog.cancel();
                                        }
                                    });

                            builder1.setNegativeButton("Tidak",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.cancel();
                                        }
                                    });

                            AlertDialog alert11 = builder1.create();
                            alert11.show();
                        }
                    });
                    alertDialog.show();

                    return false;
                }
            });

        }

        @Override
        public int getItemCount() {
            return kecamatanList.size();
        }

        @Override
        public Filter getFilter() {
            if(filter == null){
                filter = new KecamatanFilter(filterlist, this);
            }
            return filter;
        }
    }

    private class KecamatanFilter extends Filter {

        private KecamatanAdapter adapter;
        private ArrayList<Kecamatan> filterList;

        public KecamatanFilter(ArrayList<Kecamatan> filterList, KecamatanAdapter adapter){
            this.filterList = filterList;
            this.adapter = adapter;
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();
            //check constraint
            if(constraint != null && constraint.length() > 0){
                ArrayList<Kecamatan> filteredKecamatan = new ArrayList<>();

                for (int i = 0; i < filterList.size(); i++){
                    if (filterList.get(i).getNama_kecamatan().toUpperCase().contains(constraint.toString().toUpperCase())
                            | filterList.get(i).getNama_kecamatan().toUpperCase().contains(constraint.toString().toUpperCase())
                            ){
                        filteredKecamatan.add(filterList.get(i));
                    }
                }
                results.count = filteredKecamatan.size();
                results.values = filteredKecamatan;
            } else {
                results.count = filterList.size();
                results.values = filterList;
            }
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            adapter.kecamatanList = (ArrayList<Kecamatan>) results.values;
            if(results.count > 0){
                nodata.setVisibility(View.GONE);
            } else {
                nodata.setText("Data tidak ditemukan");
                nodata.setVisibility(View.VISIBLE);
            }
            adapter.notifyDataSetChanged();

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.tambah, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.tambah) {
            Intent intent = new Intent(DataKecamatanActivity.this, TambahKecamatanActivity.class);
            startActivity(intent);
            return true;
        } else if (id == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }
}
