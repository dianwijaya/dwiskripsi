package com.dwi.dwiskripsi.admin.kecamatan;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.dwi.dwiskripsi.App;
import com.dwi.dwiskripsi.Loading;
import com.dwi.dwiskripsi.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.dwi.dwiskripsi.Config.TAMBAHKECAMATAN;
import static com.dwi.dwiskripsi.Config.UPDATEKECAMATAN;

public class EditKecamatanActivity extends AppCompatActivity {

    private Button tambah;
    private EditText Enamakecamatan;
    private Loading load;
    private String nama_kecamatan, id_kecamatan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_kecamatan);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("Edit Kecamatan");

        load = new Loading(this);

        id_kecamatan = getIntent().getExtras().getString("id_kecamatan");
        nama_kecamatan = getIntent().getExtras().getString("nama_kecamatan");

        tambah = (Button) findViewById(R.id.tambah);
        Enamakecamatan = (EditText) findViewById(R.id.nama_kecamatan);
        Enamakecamatan.setText(nama_kecamatan);

        tambah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Enamakecamatan.getText().toString().isEmpty()){
                    Enamakecamatan.setError("Harus diisi.");
                } else {
                    EditKecamatan();
                }
            }
        });
    }

    private void EditKecamatan(){
        load.showpDialog();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, UPDATEKECAMATAN,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("ResponseTambahKecamatan", response);
                        if (response != null) {
                            try {
                                JSONObject Jsondata = new JSONObject(response);
                                String status = Jsondata.getString("status");
                                if (status.equals("0")) {
                                    Toast.makeText(EditKecamatanActivity.this, "Maaf, gagal menambahkan data :(" , Toast.LENGTH_SHORT);
                                } else {
                                    Toast.makeText(EditKecamatanActivity.this, "Berhasil menambahkan data :)" , Toast.LENGTH_SHORT);
                                    finish();
                                }
                            } catch (JSONException e) {
                                Log.e("ErroJSon", e.toString());
                            }
                        }
                        load.hidepDialog();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("ERRORTAMBAHKECAMATAN", error.toString());
                        load.hidepDialog();
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("nama_kecamatan", Enamakecamatan.getText().toString());
                params.put("id_kecamatan", id_kecamatan);
                return params;
            }
        };

        App.getInstance().addToRequestQueue(stringRequest);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                onBackPressed();
        }
        return (super.onOptionsItemSelected(menuItem));
    }
}