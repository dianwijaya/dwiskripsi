package com.dwi.dwiskripsi.admin.laporan;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.dwi.dwiskripsi.App;
import com.dwi.dwiskripsi.ImagePreviewActivity;
import com.dwi.dwiskripsi.Loading;
import com.dwi.dwiskripsi.R;
import com.dwi.dwiskripsi.admin.pendaftar.DetailPendaftarActivity;
import com.dwi.dwiskripsi.admin.pendaftar.TambahPendaftarActivity;
import com.dwi.dwiskripsi.model.Pendaftar;
import com.dwi.dwiskripsi.model.Unit;
import com.dwi.dwiskripsi.unit.DaftarPendaftarActivity;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.dwi.dwiskripsi.Config.GETPENDAFTARBYID_UNIT;
import static com.dwi.dwiskripsi.Config.GETUNIT;
import static com.dwi.dwiskripsi.Config.ID_LOGIN;
import static com.dwi.dwiskripsi.Config.URL_FOTO_PENDAFTAR;

public class LaporanUnitActivity extends AppCompatActivity {

    private Loading load;
    private RecyclerView recyclerView;
    private ArrayList<Unit> unitList = new ArrayList<>();
    private Spinner Sunit;
    private String id_login;
    private PendaftarAdapter adapter;
    private ArrayList<Pendaftar> pendaftarList = new ArrayList<>();
    private SwipeRefreshLayout swipe;
    private TextView nodata, Tcountsiswa;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_laporan_unit);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("Laporan Tempat Les");
        load = new Loading(this);

        swipe = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh);
        nodata = (TextView) findViewById(R.id.nodata);
        Tcountsiswa = (TextView) findViewById(R.id.countsiswa);
        Sunit = (Spinner) findViewById(R.id.unit);

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        adapter = new PendaftarAdapter(pendaftarList);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);

        swipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getPendaftar(id_login);
            }
        });

        populatingUnit();
    }

    private void populatingUnit(){
        load.showpDialog();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, GETUNIT,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("ResponseUnit", response);
                        if (response != null) {
                            try {
                                JSONObject Jsondata = new JSONObject(response);
                                JSONArray feedArray = Jsondata.getJSONArray("data");

                                for (int i = 0; i < feedArray.length(); i++) {
                                    JSONObject data = (JSONObject) feedArray.get(i);

                                    String id_unit = data.has("id_unit") ? data.getString("id_unit") : "";
                                    String nama_unit = data.has("nama_unit") ? data.getString("nama_unit") : "";
                                    String alamat = data.has("alamat") ? data.getString("alamat") : "";
                                    String telp = data.has("telp") ? data.getString("telp") : "";
                                    String gambar = data.has("gambar") ? data.getString("gambar") : "";
                                    String url = data.has("url") ? data.getString("url") : "";
                                    String deskripsi = data.has("deskripsi") ? data.getString("deskripsi") : "";
                                    String latitude = data.has("latitude") ? data.getString("latitude") : "";
                                    String longitude = data.has("longitude") ? data.getString("longitude") : "";
                                    String id_login = data.has("id_login") ? data.getString("id_login") : "";
                                    String id_kecamatan = data.has("id_kecamatan") ? data.getString("id_kecamatan") : "";
                                    String nama_kecamatan = data.has("nama_kecamatan") ? data.getString("nama_kecamatan") : "";
                                    String username = data.has("username") ? data.getString("username") : "";
                                    String password = data.has("password") ? data.getString("password") : "";


                                    Unit a = new Unit(id_unit, nama_unit, alamat, telp, gambar,
                                            url, deskripsi, latitude, longitude, id_login, id_kecamatan, nama_kecamatan, username, password);
                                    unitList.add(a);


                                }

                                List<String> lables = new ArrayList<String>();
                                for (int i = 0; i < unitList.size(); i++) {
                                    lables.add(unitList.get(i).getNama_unit());
                                }

                                final ArrayAdapter<String> adapter = new ArrayAdapter<String> (LaporanUnitActivity.this, android.R.layout.simple_spinner_item, lables);
                                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                Sunit.setAdapter(adapter);
                                Sunit.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                    @Override
                                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                        getPendaftar(unitList.get(position).getId_login());
                                        id_login = unitList.get(position).getId_login();
                                    }

                                    @Override
                                    public void onNothingSelected(AdapterView<?> parent) {

                                    }
                                });
                            } catch (JSONException e) {
                                Log.e("ErroJSon", e.toString());
                            }
                        }
                        load.hidepDialog();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("ERRORUNIT", error.toString());
                        Toast.makeText(LaporanUnitActivity.this, "Gagal mendapatkan data. Periksa koneksi Anda.", Toast.LENGTH_SHORT).show();
                        load.hidepDialog();
                    }
                });
        App.getInstance().addToRequestQueue(stringRequest);
    }

    public void getPendaftar(final String id_login) {
        load.showpDialog();
        StringRequest jsonReq = new StringRequest(Request.Method.POST, GETPENDAFTARBYID_UNIT, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("ResponsePendaftar", response.toString());
                if (response != null) {
                    try {
                        pendaftarList.clear();
                        JSONObject Jsondata = new JSONObject(response);
                        String status = Jsondata.getString("status");
                        if (status.equals("0")) {
                            nodata.setVisibility(View.VISIBLE);
                        } else {
                            nodata.setVisibility(View.GONE);
                            try {
                                JSONArray feedArray = Jsondata.getJSONArray("data");

                                for (int i = 0; i < feedArray.length(); i++) {
                                    JSONObject data = (JSONObject) feedArray.get(i);

                                    String id_pendaftar = data.has("id_pendaftar") ? data.getString("id_pendaftar") : "";
                                    String nama_lengkap = data.has("nama_lengkap") ? data.getString("nama_lengkap") : "";
                                    String nama_pendek = data.has("nama_pendek") ? data.getString("nama_pendek") : "";
                                    String tempat_lahir = data.has("tempat_lahir") ? data.getString("tempat_lahir") : "";
                                    String tanggal_lahir = data.has("tanggal_lahir") ? data.getString("tanggal_lahir") : "";
                                    String agama = data.has("agama") ? data.getString("agama") : "";
                                    String nama_ortu = data.has("nama_ortu") ? data.getString("nama_ortu") : "";
                                    String alamat = data.has("alamat") ? data.getString("alamat") : "";
                                    String telp = data.has("telp") ? data.getString("telp") : "";
                                    String nama_sekolah = data.has("nama_sekolah") ? data.getString("nama_sekolah") : "";
                                    String kelas = data.has("kelas") ? data.getString("kelas") : "";
                                    String id_login = data.has("id_login") ? data.getString("id_login") : "";
                                    String username = data.has("kelas") ? data.getString("username") : "";
                                    String password = data.has("password") ? data.getString("password") : "";
                                    String level = data.has("level") ? data.getString("level") : "";
                                    String status2 = data.has("status") ? data.getString("status") : "";
                                    String id_unit = data.has("id_unit") ? data.getString("id_unit") : "";
                                    String nama_unit = data.has("nama_unit") ? data.getString("nama_unit") : "";
                                    String gambar = data.has("gambar") ? data.getString("gambar") : "";


                                    Pendaftar a = new Pendaftar(id_pendaftar, nama_lengkap, nama_pendek,
                                            tempat_lahir, tanggal_lahir, agama, nama_ortu, alamat, telp, nama_sekolah,
                                            kelas, id_login, username, password, level, status2, id_unit,nama_unit,gambar);
                                    pendaftarList.add(a);

                                }
                                adapter.notifyDataSetChanged();
                                Tcountsiswa.setText(String.valueOf(adapter.getItemCount()));
                            } catch (JSONException e) {
                                Log.e("ERROJSON", e.toString());
                            }
                        }
                    } catch (JSONException e) {
                        Log.e("ERROJSON", e.toString());
                    }
                }
                load.hidepDialog();
                swipe.setRefreshing(false);
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("ERRORpendaftar", error.toString());
                pendaftarList.clear();
                nodata.setText(getString(R.string.noresponse));
                nodata.setVisibility(View.VISIBLE);
                load.hidepDialog();
                swipe.setRefreshing(false);
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("id_login", id_login);
                return params;
            }
        };
        App.getInstance().addToRequestQueue(jsonReq);
    }

    private class PendaftarAdapter extends RecyclerView.Adapter<PendaftarAdapter.MyViewHolder> {

        private List<Pendaftar> pendaftarList;
        ArrayList<Pendaftar> filterlist;

        public class MyViewHolder extends RecyclerView.ViewHolder {
            private TextView Tnama_lengkap;
            private LinearLayout linear;
            private CircleImageView foto;

            public MyViewHolder(View view) {
                super(view);

                Tnama_lengkap = (TextView) view.findViewById(R.id.nama_lengkap);
                linear = (LinearLayout) view.findViewById(R.id.linear);
                foto = (CircleImageView) view.findViewById(R.id.gambar);
            }
        }

        public PendaftarAdapter(ArrayList<Pendaftar> pendaftarList) {
            this.pendaftarList = pendaftarList;
            this.filterlist = pendaftarList;
        }

        @Override
        public PendaftarAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.siswa_layout, parent, false);
            return new PendaftarAdapter.MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(final PendaftarAdapter.MyViewHolder holder, final int position) {
            final Pendaftar pendaftar = pendaftarList.get(position);

            holder.Tnama_lengkap.setText(pendaftar.getNama_lengkap());
            if(pendaftar.getGambar() != null && !pendaftar.getGambar().equals("")){
                Picasso.with(LaporanUnitActivity.this)
                        .load(URL_FOTO_PENDAFTAR+pendaftar.getGambar())
                        .placeholder(R.drawable.ic_loading)
                        .error(R.drawable.ic_noimage)
                        .into(holder.foto);
            } else {
                holder.foto.setImageResource(R.drawable.ic_noimage);
            }

            holder.foto.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(LaporanUnitActivity.this, ImagePreviewActivity.class);
                    intent.putExtra("url", URL_FOTO_PENDAFTAR+pendaftar.getGambar());
                    startActivity(intent);
                }
            });

            holder.linear.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(LaporanUnitActivity.this, DetailPendaftarActivity.class);
                    intent.putExtra("id_pendaftar", pendaftar.getId_pendaftar());
                    intent.putExtra("nama_lengkap", pendaftar.getNama_lengkap());
                    intent.putExtra("nama_pendek", pendaftar.getNama_pendek());
                    intent.putExtra("tempat_lahir", pendaftar.getTempat_lahir());
                    intent.putExtra("tanggal_lahir", pendaftar.getTanggal_lahir());
                    intent.putExtra("agama", pendaftar.getAgama());
                    intent.putExtra("nama_ortu", pendaftar.getNama_ortu());
                    intent.putExtra("alamat", pendaftar.getAlamat());
                    intent.putExtra("telp", pendaftar.getTelp());
                    intent.putExtra("nama_sekolah", pendaftar.getNama_sekolah());
                    intent.putExtra("kelas", pendaftar.getKelas());
                    intent.putExtra("username", pendaftar.getUsername());
                    intent.putExtra("password", pendaftar.getPassword());
                    intent.putExtra("id_login", pendaftar.getId_login());
                    intent.putExtra("status", pendaftar.getStatus());
                    intent.putExtra("nama_unit", pendaftar.getNama_unit());
                    intent.putExtra("gambar", pendaftar.getGambar());
                    intent.putExtra("from", "unit");

                    startActivity(intent);
                }
            });
        }

        @Override
        public int getItemCount() {
            return pendaftarList.size();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                onBackPressed();
        }
        return (super.onOptionsItemSelected(menuItem));
    }
}
