package com.dwi.dwiskripsi.admin.unit;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;

import com.dwi.dwiskripsi.ImagePreviewActivity;
import com.dwi.dwiskripsi.PendaftaranUnitActivity;
import com.dwi.dwiskripsi.R;
import com.dwi.dwiskripsi.admin.pendaftar.DetailPendaftarActivity;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.picasso.Picasso;

import static com.dwi.dwiskripsi.Config.URL_FOTO_PENDAFTAR;
import static com.dwi.dwiskripsi.Config.URL_FOTO_UNIT;

public class DetailUnitActivity extends AppCompatActivity {

    private TextView Tnama_unit;
    private TextView Talamat;
    private TextView Ttelp;
    private TextView Turl;
    private TextView Tdeskripsi;
    private TextView Tlatitude;
    private TextView Tlongitude;
    private TextView Tusername;
    private TextView Tpassword;
    private TextView Tkecamatan;
    private ImageView ViewGambar;

    private String id_unit;
    private String nama_unit;
    private String alamat;
    private String telp;
    private String gambar;
    private String url;
    private String deskripsi;
    private String latitude;
    private String longitude;
    private String id_login;
    private String username;
    private String password;
    private String nama_kecamatan;
    private String from;

    //maps
    private double lati, longi;
    private MapView mapView;
    private GoogleMap googleMap;
    private ScrollView mainScrollView;

    private CardView panellogin;
    private Button daftar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_unit);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        id_unit = getIntent().getExtras().getString("id_unit");
        nama_unit = getIntent().getExtras().getString("nama_unit");
        alamat = getIntent().getExtras().getString("alamat");
        telp = getIntent().getExtras().getString("telp");
        gambar = getIntent().getExtras().getString("gambar");
        url = getIntent().getExtras().getString("url");
        deskripsi = getIntent().getExtras().getString("deskripsi");
        latitude = getIntent().getExtras().getString("latitude");
        longitude = getIntent().getExtras().getString("longitude");
        id_login = getIntent().getExtras().getString("id_login");
        username = getIntent().getExtras().getString("username");
        password = getIntent().getExtras().getString("password");
        nama_kecamatan = getIntent().getExtras().getString("nama_kecamatan");
        from = getIntent().getExtras().getString("from");

        setTitle(nama_unit);

        panellogin = (CardView) findViewById(R.id.panellogin);
        daftar = (Button) findViewById(R.id.daftar);

        if(from.equalsIgnoreCase("admin")){
            panellogin.setVisibility(View.VISIBLE);
            daftar.setVisibility(View.GONE);
        } else if(from.equals("pendaftar")){
            panellogin.setVisibility(View.GONE);
            daftar.setVisibility(View.VISIBLE);
        } else {
            panellogin.setVisibility(View.GONE);
            daftar.setVisibility(View.GONE);
        }


        Tnama_unit = (TextView) findViewById(R.id.nama_unit);
        Talamat = (TextView) findViewById(R.id.alamat);
        Ttelp = (TextView) findViewById(R.id.telp);
        Turl = (TextView) findViewById(R.id.url);
        Tdeskripsi = (TextView) findViewById(R.id.deskripsi);
        Tlatitude = (TextView) findViewById(R.id.latitude);
        Tlongitude = (TextView) findViewById(R.id.longitude);
        Tusername = (TextView) findViewById(R.id.username);
        Tpassword = (TextView) findViewById(R.id.password);
        Tkecamatan = (TextView) findViewById(R.id.kecamatan);
        ViewGambar = (ImageView) findViewById(R.id.gambar);

        Tnama_unit.setText(nama_unit);
        Talamat.setText(alamat);
        Ttelp.setText(telp);
        Turl.setText(url);
        Tdeskripsi.setText(deskripsi);
        Tlatitude.setText(latitude);
        Tlongitude.setText(longitude);
        Tusername.setText(username);
        Tpassword.setText(password);
        Tkecamatan.setText(nama_kecamatan);

        if(gambar != null && !gambar.equals("")){
            Picasso.with(this)
                    .load(URL_FOTO_UNIT+gambar)
                    .placeholder(R.drawable.ic_loading)
                    .error(R.drawable.ic_image)
                    .into(ViewGambar);
        } else {
            ViewGambar.setImageResource(R.drawable.ic_image);
        }

        ViewGambar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(DetailUnitActivity.this, ImagePreviewActivity.class);
                intent.putExtra("url", URL_FOTO_UNIT+gambar);
                startActivity(intent);
            }
        });

        try {
            lati = Double.parseDouble(latitude);
        } catch (NumberFormatException e) {
            lati = 0.000;
        }

        try {
            longi = Double.parseDouble(longitude);
        } catch (NumberFormatException e) {
            longi = 0.000;
        }

        mainScrollView = (ScrollView) findViewById(R.id.scrollView);
        mapView = (MapView) findViewById(R.id.map);
        mapView.onCreate(savedInstanceState);

        mapView.onResume();

        try {
            MapsInitializer.initialize(this);
        } catch (Exception e) {
            e.printStackTrace();
        }

        mapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap mMap) {
                googleMap = mMap;
                LatLng lokasi = new LatLng(lati, longi);
                googleMap.addMarker(new MarkerOptions().position(lokasi).title(nama_unit).snippet(alamat));
                CameraPosition cameraPosition = new CameraPosition.Builder().target(lokasi).zoom(18).build();
                googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            }
        });

        mainScrollView = (ScrollView) findViewById(R.id.scrollView);
        ImageView transparentImageView = (ImageView) findViewById(R.id.transparent_image);

        transparentImageView.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                    case MotionEvent.ACTION_DOWN:
                        mainScrollView.requestDisallowInterceptTouchEvent(true);
                        return false;
                    case MotionEvent.ACTION_UP:
                        mainScrollView.requestDisallowInterceptTouchEvent(false);
                        return true;
                    case MotionEvent.ACTION_MOVE:
                        mainScrollView.requestDisallowInterceptTouchEvent(true);
                        return false;
                    default:
                        return true;
                }
            }
        });

        daftar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DetailUnitActivity.this, PendaftaranUnitActivity.class);
                intent.putExtra("id_unit", id_unit);
                intent.putExtra("nama_unit", nama_unit);
                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                onBackPressed();
        }
        return (super.onOptionsItemSelected(menuItem));
    }
}
