package com.dwi.dwiskripsi.admin.pendaftar;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dwi.dwiskripsi.Helper;
import com.dwi.dwiskripsi.ImagePreviewActivity;
import com.dwi.dwiskripsi.R;
import com.dwi.dwiskripsi.admin.admin.DataAdminActivity;
import com.dwi.dwiskripsi.pendaftar.ProfilPendaftarActivity;
import com.squareup.picasso.Picasso;

import static com.dwi.dwiskripsi.Config.URL_FOTO_ADMIN;
import static com.dwi.dwiskripsi.Config.URL_FOTO_PENDAFTAR;

public class DetailPendaftarActivity extends AppCompatActivity {

    private TextView Tnama_lengkap, Tnama_pendek, Ttempat_lahir, Ttanggal_lahir,
            Tagama, Tnama_ortu, Talamat, Ttelp, Tnama_sekolah, Tkelas, Tusername,
            Tpassword, Tstatus, Tnama_unit;


    private String id_pendaftar;
    private String nama_lengkap;
    private String nama_pendek;
    private String tempat_lahir;
    private String tanggal_lahir;
    private String agama;
    private String nama_ortu;
    private String alamat;
    private String telp;
    private String nama_sekolah;
    private String kelas;
    private String username;
    private String password;
    private String id_login;
    private String status;
    private String nama_unit;
    private String gambar;
    private String from;
    private ImageView foto;
    private LinearLayout panelpassword;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_pendaftar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        id_pendaftar = getIntent().getExtras().getString("id_pendaftar");
        nama_lengkap = getIntent().getExtras().getString("nama_lengkap");
        nama_pendek = getIntent().getExtras().getString("nama_pendek");
        tempat_lahir = getIntent().getExtras().getString("tempat_lahir");
        tanggal_lahir = getIntent().getExtras().getString("tanggal_lahir");
        agama = getIntent().getExtras().getString("agama");
        nama_ortu = getIntent().getExtras().getString("nama_ortu");
        alamat = getIntent().getExtras().getString("alamat");
        telp = getIntent().getExtras().getString("telp");
        nama_sekolah = getIntent().getExtras().getString("nama_sekolah");
        kelas = getIntent().getExtras().getString("kelas");
        username = getIntent().getExtras().getString("username");
        password = getIntent().getExtras().getString("password");
        id_login = getIntent().getExtras().getString("id_login");
        status = getIntent().getExtras().getString("status");
        nama_unit = getIntent().getExtras().getString("nama_unit");
        gambar = getIntent().getExtras().getString("gambar");
        from = getIntent().getExtras().getString("from");

        setTitle(nama_lengkap);

        Tnama_lengkap = (TextView) findViewById(R.id.nama_lengkap);
        Tnama_pendek = (TextView) findViewById(R.id.nama_pendek);
        Ttempat_lahir = (TextView) findViewById(R.id.tempat_lahir);
        Ttanggal_lahir = (TextView) findViewById(R.id.tanggal_lahir);
        Tagama = (TextView) findViewById(R.id.agama);
        Tnama_ortu = (TextView) findViewById(R.id.nama_ortu);
        Talamat = (TextView) findViewById(R.id.alamat);
        Ttelp = (TextView) findViewById(R.id.telp);
        Tnama_sekolah = (TextView) findViewById(R.id.nama_sekolah);
        Tkelas = (TextView) findViewById(R.id.kelas);
        Tusername = (TextView) findViewById(R.id.username);
        Tpassword = (TextView) findViewById(R.id.password);
        Tstatus = (TextView) findViewById(R.id.status);
        Tnama_unit = (TextView) findViewById(R.id.nama_unit);
        foto = (ImageView) findViewById(R.id.image);
        panelpassword = (LinearLayout) findViewById(R.id.panelpassword);


        Tnama_lengkap.setText(nama_lengkap);
        Tnama_pendek.setText(nama_pendek);
        Ttempat_lahir.setText(tempat_lahir);
        Ttanggal_lahir.setText(Helper.convertDate(tanggal_lahir,"yyyy-MM-dd","dd MMMM yyyy"));
        Tagama.setText(agama);
        Tnama_ortu.setText(nama_ortu);
        Talamat.setText(alamat);
        Ttelp.setText(telp);
        Tnama_sekolah.setText(nama_sekolah);
        Tkelas.setText(kelas);
        Tusername.setText(username);
        Tpassword.setText(password);
        Tstatus.setText(status);
        Tnama_unit.setText(nama_unit);

        if(gambar != null && !gambar.equals("")){
            Picasso.with(DetailPendaftarActivity.this)
                    .load(URL_FOTO_PENDAFTAR+gambar)
                    .placeholder(R.drawable.ic_loading)
                    .error(R.drawable.ic_image)
                    .into(foto);
        } else {
            foto.setImageResource(R.drawable.ic_image);
        }

        foto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(DetailPendaftarActivity.this, ImagePreviewActivity.class);
                intent.putExtra("url", URL_FOTO_PENDAFTAR+gambar);
                startActivity(intent);
            }
        });

        if(from.equals("unit")){
            panelpassword.setVisibility(View.GONE);
        } else if(from.equals("admin")){
            panelpassword.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                onBackPressed();
        }
        return (super.onOptionsItemSelected(menuItem));
    }
}
