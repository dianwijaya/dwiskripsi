package com.dwi.dwiskripsi.admin.admin;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.dwi.dwiskripsi.App;
import com.dwi.dwiskripsi.Loading;
import com.dwi.dwiskripsi.R;
import com.dwi.dwiskripsi.admin.unit.TambahUnitActivity;
import com.soundcloud.android.crop.Crop;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static com.dwi.dwiskripsi.Config.TAMBAHADMIN;
import static com.dwi.dwiskripsi.Config.TAMBAHKECAMATAN;

public class TambahAdminActivity extends AppCompatActivity {

    private Button tambah;
    private EditText Enama, Ealamat, Eusername, Epassword;
    private Loading load;

    //photo
    private Uri imageUriCamera;
    private int REQUEST_CAMERA = 3;
    private ImageView ViewGambar;
    private String hasilencodegambar = "";
    private Button pilihgambar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tambah_admin);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("Tambah Admin Pusat");

        load = new Loading(this);

        tambah = (Button) findViewById(R.id.tambah);
        Enama = (EditText) findViewById(R.id.nama);
        Ealamat = (EditText) findViewById(R.id.alamat);
        Eusername = (EditText) findViewById(R.id.username);
        Epassword = (EditText) findViewById(R.id.password);

        tambah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Enama.getText().toString().isEmpty()){
                    Enama.setError("Harus diisi.");
                } else {
                    TambahAdmin();
                }
            }
        });


        ViewGambar = (ImageView) findViewById(R.id.gambar);
        pilihgambar = (Button) findViewById(R.id.pilihgambar);
        pilihgambar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImage();
            }
        });

    }

    private void TambahAdmin(){
        load.showpDialog();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, TAMBAHADMIN,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("ResponseTambahAdmin", response);
                        if (response != null) {
                            try {
                                JSONObject Jsondata = new JSONObject(response);
                                String status = Jsondata.getString("status");
                                if (status.equals("0")) {
                                    Toast.makeText(TambahAdminActivity.this, "Maaf, gagal menambahkan data :(" , Toast.LENGTH_SHORT);
                                }else {
                                    Toast.makeText(TambahAdminActivity.this, "Berhasil menambahkan data :)" , Toast.LENGTH_SHORT);
                                    finish();
                                }
                            } catch (JSONException e) {
                                Log.e("ErroJSon", e.toString());
                            }
                        }
                        load.hidepDialog();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("ERRORTAMBAHAdmin", error.toString());
                        load.hidepDialog();
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("nama", Enama.getText().toString());
                params.put("alamat", Ealamat.getText().toString());
                params.put("username", Eusername.getText().toString());
                params.put("password", Epassword.getText().toString());
                params.put("gambar", hasilencodegambar);
                return params;
            }
        };
        App.getInstance().addToRequestQueue(stringRequest);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                onBackPressed();
        }
        return (super.onOptionsItemSelected(menuItem));
    }

    private void selectImage() {

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(TambahAdminActivity.this);

        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View dialogView = inflater.inflate(R.layout.dialog_upload_foto, null);
        dialogBuilder.setView(dialogView);

        Button kamera = (Button) dialogView.findViewById(R.id.kamera);
        Button gallery = (Button) dialogView.findViewById(R.id.galery);

        final AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.setCancelable(true);

        kamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
                if(imageUriCamera != null){
                    imageUriCamera = null;
                }
                takePhoto();
            }
        });

        gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
                Crop.pickImage(TambahAdminActivity.this);
            }
        });
        alertDialog.show();
    }

    public void takePhoto() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File photo = new File(Environment.getExternalStorageDirectory(), "png.png");
        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photo));
        imageUriCamera = Uri.fromFile(photo);
        startActivityForResult(intent, REQUEST_CAMERA);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == REQUEST_CAMERA && resultCode == RESULT_OK){
            getContentResolver().notifyChange(imageUriCamera , null);
            beginCrop(imageUriCamera);
        } else if (requestCode == Crop.REQUEST_PICK && resultCode == RESULT_OK) {
            beginCrop(data.getData());
        } else if (requestCode == Crop.REQUEST_CROP) {
            handleCrop(resultCode, data);
        }
    }

    private void beginCrop(Uri source) {
        Uri destination = Uri.fromFile(new File(getCacheDir(), "cropped"));
        Crop.of(source, destination).withMaxSize(800, 800).start(this);
    }

    private void handleCrop(int resultCode, Intent result) {
        if (resultCode == RESULT_OK) {
            ViewGambar.setImageDrawable(null);
            ViewGambar.setImageURI(Crop.getOutput(result));

            try {
                Bitmap bitmapcrop = MediaStore.Images.Media.getBitmap(getContentResolver(), Crop.getOutput(result));
                hasilencodegambar = encodeImage(bitmapcrop);
                Log.e("GAMBAR", hasilencodegambar);

            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (resultCode == Crop.RESULT_ERROR) {
            Toast.makeText(this, Crop.getError(result).getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private String encodeImage(Bitmap bm) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.JPEG,90,baos);
        byte[] b = baos.toByteArray();
        String encImage = Base64.encodeToString(b, Base64.DEFAULT);
        return encImage;
    }
}