package com.dwi.dwiskripsi.pendaftar;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.dwi.dwiskripsi.App;
import com.dwi.dwiskripsi.ImagePreviewActivity;
import com.dwi.dwiskripsi.Loading;
import com.dwi.dwiskripsi.LoginActivity;
import com.dwi.dwiskripsi.R;
import com.dwi.dwiskripsi.admin.unit.DetailUnitActivity;
import com.dwi.dwiskripsi.unit.DaftarPendaftarActivity;
import com.dwi.dwiskripsi.unit.EditProfileUnitActivity;
import com.dwi.dwiskripsi.unit.ProfileUnitActivity;
import com.dwi.dwiskripsi.unit.UnitActivity;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.picasso.Picasso;

import android.support.design.widget.Snackbar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.dwi.dwiskripsi.Config.GETUNITBYID;
import static com.dwi.dwiskripsi.Config.GETUNITBYIDPENDAFTAR;
import static com.dwi.dwiskripsi.Config.ID_LOGIN;
import static com.dwi.dwiskripsi.Config.URL_FOTO_UNIT;
import static com.dwi.dwiskripsi.Config.USERNAME;

public class PendaftarActivity extends AppCompatActivity {

    private Button profil, dataunit;
    private boolean backPressToExit = false;
    private Loading load;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);
        //setTitle(App.getData(USERNAME));
        setTitle("Siswa");
        load = new Loading(this);
        profil = (Button) findViewById(R.id.profil);
        dataunit = (Button) findViewById(R.id.dataunit);

        profil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PendaftarActivity.this, ProfilPendaftarActivity.class);
                startActivity(intent);
            }
        });

        dataunit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Intent intent = new Intent(PendaftarActivity.this, PendaftaranUnitActivity.class);
                //startActivity(intent);
                getUnit();
            }
        });
    }

    public void getUnit() {
        load.showpDialog();
        StringRequest jsonReq = new StringRequest(Request.Method.POST, GETUNITBYIDPENDAFTAR, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("ResponseUnit", response.toString());
                if (response != null) {
                    try {
                        JSONObject Jsondata = new JSONObject(response);
                        String status = Jsondata.getString("status");

                        try {
                            JSONArray feedArray = Jsondata.getJSONArray("data");

                            for (int i = 0; i < 1; i++) {
                                JSONObject data = (JSONObject) feedArray.get(i);

                                final String id_unit = data.has("id_unit") ? data.getString("id_unit") : "";
                                final String nama_unit = data.has("nama_unit") ? data.getString("nama_unit") : "";
                                final String alamat = data.has("alamat") ? data.getString("alamat") : "";
                                final String telp = data.has("telp") ? data.getString("telp") : "";
                                final String gambar = data.has("gambar") ? data.getString("gambar") : "";
                                final String url = data.has("url") ? data.getString("url") : "";
                                final String deskripsi = data.has("deskripsi") ? data.getString("deskripsi") : "";
                                final String latitude = data.has("latitude") ? data.getString("latitude") : "";
                                final String longitude = data.has("longitude") ? data.getString("longitude") : "";
                                final String id_login = data.has("id_login") ? data.getString("id_login") : "";
                                final String id_kecamatan = data.has("id_kecamatan") ? data.getString("id_kecamatan") : "";
                                String nama_kecamatan = data.has("nama_kecamatan") ? data.getString("nama_kecamatan") : "";
                                final String username = data.has("username") ? data.getString("username") : "";
                                final String password = data.has("password") ? data.getString("password") : "";

                                Intent intent = new Intent(PendaftarActivity.this, DetailUnitActivity.class);
                                intent.putExtra("id_unit", id_unit);
                                intent.putExtra("nama_unit", nama_unit );
                                intent.putExtra("alamat", alamat);
                                intent.putExtra("telp", telp);
                                intent.putExtra("gambar", gambar);
                                intent.putExtra("url", url);
                                intent.putExtra("deskripsi", deskripsi);
                                intent.putExtra("latitude", latitude);
                                intent.putExtra("longitude", longitude);
                                intent.putExtra("id_login", id_login);
                                intent.putExtra("username", username);
                                intent.putExtra("password", password);
                                intent.putExtra("nama_kecamatan", nama_kecamatan);
                                intent.putExtra("from","pendaftar");
                                startActivity(intent);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                load.hidepDialog();
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("ERRORUNIT", error.toString());
                load.hidepDialog();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("id_login", App.getData(ID_LOGIN));
                return params;
            }
        };
        App.getInstance().addToRequestQueue(jsonReq);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.logout, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.logout) {
            App.logout();
            Intent intent = new Intent(PendaftarActivity.this, LoginActivity.class);
            startActivity(intent);
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {

        if (backPressToExit) {
            super.onBackPressed();
            finish();
            return;
        }
        this.backPressToExit = true;
        Snackbar.make(findViewById(R.id.myView), "Tekan sekali lagi untuk keluar.", Snackbar.LENGTH_SHORT).show();
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                backPressToExit = false;
            }
        }, 2000);
    }
}
