package com.dwi.dwiskripsi.pendaftar;

import android.content.Intent;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.dwi.dwiskripsi.App;
import com.dwi.dwiskripsi.Helper;
import com.dwi.dwiskripsi.ImagePreviewActivity;
import com.dwi.dwiskripsi.Loading;
import com.dwi.dwiskripsi.R;
import com.dwi.dwiskripsi.admin.pendaftar.DataPendaftarActivity;
import com.dwi.dwiskripsi.admin.pendaftar.EditPendaftarActivity;
import com.dwi.dwiskripsi.unit.EditProfileUnitActivity;
import com.dwi.dwiskripsi.unit.ProfileUnitActivity;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.dwi.dwiskripsi.Config.GETPENDAFTARBYID;
import static com.dwi.dwiskripsi.Config.ID_LOGIN;
import static com.dwi.dwiskripsi.Config.URL_FOTO_PENDAFTAR;
import static com.dwi.dwiskripsi.Config.URL_FOTO_UNIT;

public class ProfilPendaftarActivity extends AppCompatActivity {

    private TextView Tnama_lengkap, Tnama_pendek, Ttempat_lahir, Ttanggal_lahir,
            Tagama, Tnama_ortu, Talamat, Ttelp, Tnama_sekolah, Tkelas, Tusername,
            /*Tpassword,*/ Tstatus, Tnama_unit;
    private ImageView foto;
    private Loading load;
    private SwipeRefreshLayout swipe;
    private Button editprofil;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profil_pendaftar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("Profil");

        load = new Loading(this);

        editprofil = (Button) findViewById(R.id.editprofil);
        swipe = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh);
        Tnama_lengkap = (TextView) findViewById(R.id.nama_lengkap);
        Tnama_pendek = (TextView) findViewById(R.id.nama_pendek);
        Ttempat_lahir = (TextView) findViewById(R.id.tempat_lahir);
        Ttanggal_lahir = (TextView) findViewById(R.id.tanggal_lahir);
        Tagama = (TextView) findViewById(R.id.agama);
        Tnama_ortu = (TextView) findViewById(R.id.nama_ortu);
        Talamat = (TextView) findViewById(R.id.alamat);
        Ttelp = (TextView) findViewById(R.id.telp);
        Tnama_sekolah = (TextView) findViewById(R.id.nama_sekolah);
        Tkelas = (TextView) findViewById(R.id.kelas);
        Tusername = (TextView) findViewById(R.id.username);
        //Tpassword = (TextView) findViewById(R.id.password);
        Tstatus = (TextView) findViewById(R.id.status);
        Tnama_unit = (TextView) findViewById(R.id.nama_unit);
        foto = (ImageView) findViewById(R.id.image);


        swipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getPendaftar();
            }
        });

    }

    @Override
    protected void onStart() {
        super.onStart();
        getPendaftar();
    }

    public void getPendaftar() {
        load.showpDialog();
        StringRequest jsonReq = new StringRequest(Request.Method.POST, GETPENDAFTARBYID, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("ResponsePendaftar", response.toString());
                if (response != null) {
                    try {
                        JSONObject Jsondata = new JSONObject(response);

                        final String status = Jsondata.getString("status");
                        if(status.equals("1")){
                            JSONArray feedArray = Jsondata.getJSONArray("data");

                            for (int i = 0; i < 1; i++) {
                                JSONObject data = (JSONObject) feedArray.get(i);

                                final String id_pendaftar = data.has("id_pendaftar") ? data.getString("id_pendaftar") : "";
                                final String nama_lengkap = data.has("nama_lengkap") ? data.getString("nama_lengkap") : "";
                                final String nama_pendek = data.has("nama_pendek") ? data.getString("nama_pendek") : "";
                                final String tempat_lahir = data.has("tempat_lahir") ? data.getString("tempat_lahir") : "";
                                final String tanggal_lahir = data.has("tanggal_lahir") ? data.getString("tanggal_lahir") : "";
                                final String agama = data.has("agama") ? data.getString("agama") : "";
                                final String nama_ortu = data.has("nama_ortu") ? data.getString("nama_ortu") : "";
                                final String alamat = data.has("alamat") ? data.getString("alamat") : "";
                                final String telp = data.has("telp") ? data.getString("telp") : "";
                                final String nama_sekolah = data.has("nama_sekolah") ? data.getString("nama_sekolah") : "";
                                final String kelas = data.has("kelas") ? data.getString("kelas") : "";
                                final String id_login = data.has("id_login") ? data.getString("id_login") : "";
                                final String username = data.has("kelas") ? data.getString("username") : "";
                                final String password = data.has("password") ? data.getString("password") : "";
                                String level = data.has("level") ? data.getString("level") : "";
                                final String status2 = data.has("stat") ? data.getString("stat") : "";
                                final String id_unit = data.has("id_unit") ? data.getString("id_unit") : "";
                                String nama_unit = data.has("nama_unit") ? data.getString("nama_unit") : "";
                                final String gambar = data.has("gambar") ? data.getString("gambar") : "";

                                Tnama_lengkap.setText(nama_lengkap);
                                Tnama_pendek.setText(nama_pendek);
                                Ttempat_lahir.setText(tempat_lahir);
                                Ttanggal_lahir.setText(Helper.convertDate(tanggal_lahir,"yyyy-MM-dd","dd MMMM yyyy"));
                                Tnama_ortu.setText(nama_ortu);
                                Talamat.setText(alamat);
                                Ttelp.setText(telp);
                                Tnama_sekolah.setText(nama_sekolah);
                                Tkelas.setText(kelas);
                                Tusername.setText(username);
                                Tagama.setText(agama);
                                //Tpassword.setText(password);
                                Tstatus.setText(status2);
                                Tnama_unit.setText(nama_unit);

                                if(gambar != null && !gambar.equals("")){
                                    Picasso.with(ProfilPendaftarActivity.this)
                                            .load(URL_FOTO_PENDAFTAR+gambar)
                                            .placeholder(R.drawable.ic_loading)
                                            .error(R.drawable.ic_image)
                                            .into(foto);
                                } else {
                                    foto.setImageResource(R.drawable.ic_image);
                                }

                                foto.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        Intent intent = new Intent(ProfilPendaftarActivity.this, ImagePreviewActivity.class);
                                        intent.putExtra("url", URL_FOTO_PENDAFTAR+gambar);
                                        startActivity(intent);
                                    }
                                });

                                editprofil.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        Intent intent = new Intent(ProfilPendaftarActivity.this, EditProfilActivity.class);
                                        intent.putExtra("id_pendaftar", id_pendaftar);
                                        intent.putExtra("nama_lengkap", nama_lengkap);
                                        intent.putExtra("nama_pendek", nama_pendek);
                                        intent.putExtra("tempat_lahir", tempat_lahir );
                                        intent.putExtra("tanggal_lahir", tanggal_lahir);
                                        intent.putExtra("agama", agama);
                                        intent.putExtra("nama_ortu", nama_ortu);
                                        intent.putExtra("alamat", alamat);
                                        intent.putExtra("telp", telp );
                                        intent.putExtra("nama_sekolah",nama_sekolah);
                                        intent.putExtra("kelas", kelas);
                                        intent.putExtra("username", username);
                                        intent.putExtra("password", password);
                                        intent.putExtra("id_login", id_login);
                                        intent.putExtra("status", status2);
                                        intent.putExtra("id_unit", id_unit);
                                        intent.putExtra("gambar", gambar);
                                        startActivity(intent);
                                    }
                                });

                            }
                        } else {

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                load.hidepDialog();
                swipe.setRefreshing(false);
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("ERRORpendaftar", error.toString());
                load.hidepDialog();
                swipe.setRefreshing(false);
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("id_login", App.getData(ID_LOGIN));
                Log.e("id_login", App.getData(ID_LOGIN));
                return params;
            }
        };
        App.getInstance().addToRequestQueue(jsonReq);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                onBackPressed();
        }
        return (super.onOptionsItemSelected(menuItem));
    }
}

