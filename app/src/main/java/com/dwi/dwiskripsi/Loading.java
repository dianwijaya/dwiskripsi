package com.dwi.dwiskripsi;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;


import org.w3c.dom.Text;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class Loading {
    private SweetAlertDialog pDialog;
    private Activity activity;
    private String text;

    public Loading(Activity activity) {
        this.activity = activity;
        text = "Loading...";
        pDialog = new SweetAlertDialog(activity, SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText(text);
        pDialog.setContentText("Harap tunggu sebentar");
        pDialog.setCancelable(false);
    }

    public void showpDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    public void hidepDialog() {
        if (!activity.isFinishing() && pDialog != null && pDialog.isShowing())
            pDialog.dismiss();
    }
}
