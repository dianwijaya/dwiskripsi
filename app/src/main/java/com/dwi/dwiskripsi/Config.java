package com.dwi.dwiskripsi;

import com.google.android.gms.maps.model.LatLng;

public class Config {
    //hosting
    public static String BASE_URL = "http://dwiew.000webhostapp.com";
    //public static String BASE_URL = "http://dianwijayaputra.hol.es";

    //local tatering
    //public static String BASE_URL = "http://192.168.0.176/api";
    //public static String BASE_URL = "http://192.168.43.171/api";

    public static String URL_FOTO_UNIT = BASE_URL+"/image/unit/";
    public static String URL_FOTO_PENDAFTAR = BASE_URL+"/image/pendaftar/";
    public static String URL_FOTO_ADMIN = BASE_URL+"/image/admin/";

    public static String LOGIN = BASE_URL+"/login.php";

    public static String GETKECAMATAN = BASE_URL+"/getkecamatan.php";
    public static String HAPUSKECAMATAN = BASE_URL+"/hapuskecamatan.php";
    public static String UPDATEKECAMATAN = BASE_URL+"/updatekecamatan.php";
    public static String TAMBAHKECAMATAN = BASE_URL+"/tambahkecamatan.php";

    public static String GETPENDAFTAR = BASE_URL+"/getpendaftar.php";
    public static String HAPUSPENDAFTAR = BASE_URL+"/hapuspendaftar.php";
    public static String UPDATEPENDAFTAR = BASE_URL+"/updatependaftar.php";
    public static String TAMBAHPENDAFTAR = BASE_URL+"/tambahpendaftar.php";

    public static String GETADMIN = BASE_URL+"/getadmin.php";
    public static String HAPUSADMIN = BASE_URL+"/hapusadmin.php";
    public static String UPDATEADMIN= BASE_URL+"/updateadmin.php";
    public static String TAMBAHADMIN = BASE_URL+"/tambahadmin.php";

    public static String GETUNIT = BASE_URL+"/getunit.php";
    public static String HAPUSUNIT = BASE_URL+"/hapusunit.php";
    public static String UPDATEUNIT= BASE_URL+"/updateunit.php";
    public static String TAMBAHUNIT = BASE_URL+"/tambahunit.php";

    public static String GETUNITBYID = BASE_URL+"/getunitbyid.php";
    public static String GETUNITBYIDPENDAFTAR = BASE_URL+"/getunitbyidpendaftar.php";
    public static String GETPENDAFTARBYID = BASE_URL+"/getpendaftarbyid.php";
    public static String GETPENDAFTARBYID_UNIT = BASE_URL+"/getpendaftarbyid_unit.php";
    public static String TANDAITERIMA = BASE_URL+"/tandaiterima.php";

    public static String PREF_NAME = "skripsi";
    public static String ID_LOGIN = "id_login";
    public static String LEVEL = "level";
    public static String USERNAME = "username";

    //DEFAULT POSITION MAPS
    public static double LATITUDE = -7.566967;
    public static double LONGITUDE = 110.811211;
    public static LatLng DEFAULT_SOLO = new LatLng(-7.555694, 110.821550);
    public static String GET_JARAK_API_GOOGLE = "https://maps.googleapis.com/maps/api/directions/json?key=AIzaSyAQsm8Oczc3DfX8Khl2Ah0cL-qQhA_fUEA&mode=driving";

}
