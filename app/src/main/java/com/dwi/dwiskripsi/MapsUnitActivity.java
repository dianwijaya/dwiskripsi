package com.dwi.dwiskripsi;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Build;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.dwi.dwiskripsi.admin.AdminActivity;
import com.dwi.dwiskripsi.admin.unit.DetailUnitActivity;
import com.dwi.dwiskripsi.model.Unit;
import com.dwi.dwiskripsi.pendaftar.PendaftarActivity;
import com.dwi.dwiskripsi.unit.UnitActivity;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import android.support.design.widget.Snackbar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Map;

import static com.dwi.dwiskripsi.Config.ID_LOGIN;
import static com.dwi.dwiskripsi.Config.LATITUDE;
import static com.dwi.dwiskripsi.Config.LEVEL;
import static com.dwi.dwiskripsi.Config.LONGITUDE;

public class MapsUnitActivity extends AppCompatActivity {

    private MapView mapView;
    private GoogleMap googleMap;
    private Loading load;
    private ArrayList<Unit> unitList = new ArrayList<>();
    private Button masuk, daftar, lihatunit;
    private boolean backPressToExit = false;
    private double latiku, longiku;
    private GetMyLocation myLocation;
    private ImageButton getMyLocation;
    private MarkerOptions Mposisiku;
    private Button lokasiterdekat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(!App.getData(ID_LOGIN).equals("")){
            if(App.getData(LEVEL).equalsIgnoreCase("admin")){
                Intent intent = new Intent(MapsUnitActivity.this, AdminActivity.class);
                startActivity(intent);
                finish();
            } else if(App.getData(LEVEL).equalsIgnoreCase("pendaftar")){
                Intent intent = new Intent(MapsUnitActivity.this, PendaftarActivity.class);
                startActivity(intent);
                finish();
            } else if(App.getData(LEVEL).equalsIgnoreCase("unit")){
                Intent intent = new Intent(MapsUnitActivity.this, UnitActivity.class);
                startActivity(intent);
                finish();
            }
        }
        setContentView(R.layout.activity_maps_unit);
        myLocation = new GetMyLocation(this);
        //getSupportActionBar().setHomeButtonEnabled(true);
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("Lokasi Tempat Les");

        //getMyLocation = (ImageButton) findViewById(R.id.getMyLocation);
        lokasiterdekat = (Button) findViewById(R.id.lokasiterdekat);
        masuk = (Button) findViewById(R.id.masuk);
        daftar = (Button) findViewById(R.id.daftar);

        lokasiterdekat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MapsUnitActivity.this, TerdekatActivity.class);
                startActivity(intent);
            }
        });

        load = new Loading(this);

        mapView = (MapView) findViewById(R.id.map);
        mapView.onCreate(savedInstanceState);

        mapView.onResume();

        try {
            MapsInitializer.initialize(this);
        } catch (Exception e) {
            e.printStackTrace();
        }

        mapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap mMap) {
                googleMap = mMap;
                getHalte();
            }
        });

        daftar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MapsUnitActivity.this, SignUpActivity.class);
                startActivity(intent);
            }
        });

        masuk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MapsUnitActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();
            }
        });

        final LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        if ( !manager.isProviderEnabled( LocationManager.GPS_PROVIDER ) ) {
            final AlertDialog.Builder builder = new AlertDialog.Builder(MapsUnitActivity.this);
            builder.setMessage("GPS tidak aktif. Anda harus menghidupkan GPS untuk mendapatkan lokasi Anda saat ini yang akurat. Apakah Anda ingin mengaktifkan GPS ?")
                    .setCancelable(false)
                    .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                        public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                            startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                        }
                    })
                    .setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                        public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                            Toast.makeText(MapsUnitActivity.this, "Anda belum mengaktifkan GPS !", Toast.LENGTH_SHORT).show();
                            dialog.cancel();
                        }
                    });
            final AlertDialog alert = builder.create();
            alert.show();
        }

       /* getMyLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int MyVersion = Build.VERSION.SDK_INT;
                if (MyVersion > Build.VERSION_CODES.LOLLIPOP_MR1) {

                    int result4 = ContextCompat.checkSelfPermission(MapsUnitActivity.this, android.Manifest.permission.ACCESS_FINE_LOCATION);
                    int result5 = ContextCompat.checkSelfPermission(MapsUnitActivity.this, android.Manifest.permission.ACCESS_COARSE_LOCATION);

                    if (result4 == PackageManager.PERMISSION_DENIED && result5 == PackageManager.PERMISSION_DENIED) {
                        ActivityCompat.requestPermissions(MapsUnitActivity.this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION}, 101);
                    } else {
                        final LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

                        if ( !manager.isProviderEnabled( LocationManager.GPS_PROVIDER ) ) {
                            final AlertDialog.Builder builder = new AlertDialog.Builder(MapsUnitActivity.this);
                            builder.setMessage("GPS tidak aktif. Anda harus menghidupkan GPS untuk mendapatkan lokasi Anda saat ini yang akurat. Apakah Anda ingin mengaktifkan GPS ?")
                                    .setCancelable(false)
                                    .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                                        public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                                            startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                                        }
                                    })
                                    .setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                                        public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                                            dialog.cancel();
                                        }
                                    });
                            final AlertDialog alert = builder.create();
                            alert.show();
                        } else {
                            myLocation.startLocationUpdates();
                            myLocation.displayLocation();
                            if(App.getData("latitude") != null && App.getData("longitude") != null) {
                                try {
                                    lati = Double.parseDouble(StorageManager.getString("latitude"));
                                    Tlatitude.setText(StorageManager.getString("latitude"));
                                } catch (NumberFormatException e) {
                                    lati = 0.000;
                                }

                                try {
                                    longi = Double.parseDouble(StorageManager.getString("longitude"));
                                    Tlongitude.setText(StorageManager.getString("longitude"));
                                } catch (NumberFormatException e) {
                                    longi = 0.000;
                                }
                                Mposisiku.up(new LatLng(lati, longi));
                                CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(lati, longi)).zoom(16).build();
                                googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                            }
                        }
                    }
                }
            }
        });*/
    }

    public void getHalte() {
        load.showpDialog();
        JsonObjectRequest jsonReq = new JsonObjectRequest(Request.Method.GET, Config.GETUNIT, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("ResponseUnit", response.toString());
                if (response != null) {
                    try {
                        String status = response.getString("status");
                        if (status.equals("1")) {

                                JSONArray feedArray = response.getJSONArray("data");

                                double lat, longi;

                                for (int i = 0; i < feedArray.length(); i++) {
                                    JSONObject data = (JSONObject) feedArray.get(i);

                                    String id_unit = data.has("id_unit") ? data.getString("id_unit") : "";
                                    String nama_unit = data.has("nama_unit") ? data.getString("nama_unit") : "";
                                    String alamat = data.has("alamat") ? data.getString("alamat") : "";
                                    String telp = data.has("telp") ? data.getString("telp") : "";
                                    String gambar = data.has("gambar") ? data.getString("gambar") : "";
                                    String url = data.has("url") ? data.getString("url") : "";
                                    String deskripsi = data.has("deskripsi") ? data.getString("deskripsi") : "";
                                    String latitude = data.has("latitude") ? data.getString("latitude") : "";
                                    String longitude = data.has("longitude") ? data.getString("longitude") : "";
                                    String id_login = data.has("id_login") ? data.getString("id_login") : "";
                                    String id_kecamatan = data.has("id_kecamatan") ? data.getString("id_kecamatan") : "";
                                    String nama_kecamatan = data.has("nama_kecamatan") ? data.getString("nama_kecamatan") : "";
                                    String username = data.has("username") ? data.getString("username") : "";
                                    String password = data.has("password") ? data.getString("password") : "";

                                    Unit a = new Unit(id_unit, nama_unit, alamat, telp, gambar,
                                            url, deskripsi, latitude, longitude, id_login, id_kecamatan, nama_kecamatan, username, password);
                                    unitList.add(a);

                                    try {
                                        lat = Double.parseDouble(latitude);
                                    } catch (NumberFormatException e) {
                                        lat = 0.000;
                                    }

                                    try {
                                        longi = Double.parseDouble(longitude);
                                    } catch (NumberFormatException e) {
                                        longi = 0.000;
                                    }

                                    LatLng lokasi = new LatLng(lat, longi);

                                    googleMap.addMarker(new MarkerOptions().position(lokasi).title(nama_unit).snippet("klik untuk melihat info tempat les").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)));
                                }

                                try {
                                    latiku = Double.parseDouble(App.getData("latitude"));
                                } catch (NumberFormatException e) {
                                    latiku = LATITUDE;
                                }

                                try {
                                    longiku = Double.parseDouble(App.getData("longitude"));
                                } catch (NumberFormatException e) {
                                    longiku = LONGITUDE;
                                }


                                LatLng lokasiku = new LatLng(latiku, longiku);
                                Mposisiku = new MarkerOptions().position(lokasiku).title("Lokasi Anda").snippet("Lokasi Anda saat ini berada").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
                                googleMap.addMarker(Mposisiku);

                                CameraPosition cameraPosition = new CameraPosition.Builder().target(Config.DEFAULT_SOLO).zoom(13).build();
                                googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                                googleMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
                                    @Override
                                    public void onInfoWindowClick(Marker marker) {
                                        for(int i = 0; i < unitList.size(); i++){
                                            if(unitList.get(i).getNama_unit().equals(marker.getTitle())){
                                                Intent intent = new Intent(MapsUnitActivity.this, DetailUnitActivity.class);
                                                intent.putExtra("id_unit", unitList.get(i).getId_unit());
                                                intent.putExtra("nama_unit", unitList.get(i).getNama_unit());
                                                intent.putExtra("alamat", unitList.get(i).getAlamat());
                                                intent.putExtra("telp", unitList.get(i).getTelp());
                                                intent.putExtra("gambar", unitList.get(i).getGambar());
                                                intent.putExtra("url", unitList.get(i).getUrl());
                                                intent.putExtra("deskripsi", unitList.get(i).getDeskripsi());
                                                intent.putExtra("latitude", unitList.get(i).getLatitude());
                                                intent.putExtra("longitude", unitList.get(i).getLongitude());
                                                intent.putExtra("id_login", unitList.get(i).getId_login());
                                                intent.putExtra("username", unitList.get(i).getUsername());
                                                intent.putExtra("password", unitList.get(i).getPassword());
                                                intent.putExtra("nama_kecamatan", unitList.get(i).getNama_kecamatan());
                                                intent.putExtra("from", "pendaftar");
                                                startActivity(intent);
                                            }
                                        }
                                    }
                                });

                        }
                    } catch (JSONException e) {
                        Log.e("JSON", e.toString());
                    }
                }
                load.hidepDialog();
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(MapsUnitActivity.this, getString(R.string.noresponse), Toast.LENGTH_SHORT).show();
                load.hidepDialog();
            }
        });
        App.getInstance().addToRequestQueue(jsonReq);
    }

    @Override
    protected void onStart() {
        super.onStart();
        myLocation.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        myLocation.onResume();
        mapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        myLocation.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    public void onBackPressed() {

        if (backPressToExit) {
            super.onBackPressed();
            finish();
            return;
        }
        this.backPressToExit = true;
        Snackbar.make(findViewById(R.id.myView), "Tekan sekali lagi untuk keluar.", Snackbar.LENGTH_SHORT).show();
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                backPressToExit = false;
            }
        }, 2000);
    }

    /*@Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                onBackPressed();
        }
        return (super.onOptionsItemSelected(menuItem));
    }*/
}