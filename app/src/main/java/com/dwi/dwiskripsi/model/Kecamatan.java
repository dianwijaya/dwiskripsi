package com.dwi.dwiskripsi.model;

public class Kecamatan {

    private String id_kecamatan;
    private String nama_kecamatan;

    public Kecamatan() {

    }

    public Kecamatan(String id_kecamatan, String nama_kecamatan) {

        this.id_kecamatan = id_kecamatan;
        this.nama_kecamatan = nama_kecamatan;

    }

    public String getId_kecamatan() {
        return id_kecamatan;
    }

    public String getNama_kecamatan() {
        return nama_kecamatan;
    }
}
