package com.dwi.dwiskripsi.model;

public class Admin {

    private String id_admin;
    private String nama;
    private String alamat;
    private String id_login;
    private String username;
    private String password;
    private String gambar;

    public Admin() {

    }

    public Admin(String id_admin, String nama, String alamat, String id_login, String username, String password, String gambar) {

        this.id_admin = id_admin;
        this.nama = nama;
        this.alamat = alamat;
        this.id_login = id_login;
        this.username = username;
        this.password = password;
        this.gambar = gambar;

    }

    public String getId_admin() {
        return id_admin;
    }

    public String getNama() {
        return nama;
    }

    public String getAlamat() {
        return alamat;
    }

    public String getId_login() {
        return id_login;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getGambar() {
        return gambar;
    }
}
