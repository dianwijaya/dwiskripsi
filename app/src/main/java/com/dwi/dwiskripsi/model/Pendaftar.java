package com.dwi.dwiskripsi.model;

public class Pendaftar {

    private String id_pendaftar;
    private String nama_lengkap;
    private String nama_pendek;
    private String tempat_lahir;
    private String tanggal_lahir;
    private String agama;
    private String nama_ortu;
    private String alamat;
    private String telp;
    private String nama_sekolah;
    private String kelas;
    private String id_login;
    private String username;
    private String password;
    private String level;
    private String status;
    private String id_unit;
    private String nama_unit;
    private String gambar;


    public Pendaftar() {

    }

    public Pendaftar(String id_pendaftar, String nama_lengkap, String nama_pendek, String tempat_lahir,
                     String tanggal_lahir, String agama, String nama_ortu, String alamat, String telp,
                     String nama_sekolah, String kelas, String id_login, String username, String password,
                     String level, String status, String id_unit, String nama_unit, String gambar) {

        this.id_pendaftar = id_pendaftar;
        this.nama_lengkap = nama_lengkap;
        this.nama_pendek = nama_pendek;
        this.tempat_lahir = tempat_lahir;
        this.tanggal_lahir = tanggal_lahir;
        this.agama = agama;
        this.nama_ortu = nama_ortu;
        this.alamat = alamat;
        this.telp = telp;
        this.nama_sekolah = nama_sekolah;
        this.kelas = kelas;
        this.id_login = id_login;
        this.username = username;
        this.password = password;
        this.level = level;
        this.status = status;
        this.id_unit = id_unit;
        this.nama_unit = nama_unit;
        this.gambar = gambar;

    }

    public String getId_pendaftar() {
        return id_pendaftar;
    }

    public String getNama_lengkap() {
        return nama_lengkap;
    }

    public String getNama_pendek() {
        return nama_pendek;
    }

    public String getTempat_lahir() {
        return tempat_lahir;
    }

    public String getTanggal_lahir() {
        return tanggal_lahir;
    }

    public String getAgama() {
        return agama;
    }

    public String getNama_ortu() {
        return nama_ortu;
    }

    public String getAlamat() {
        return alamat;
    }

    public String getTelp() {
        return telp;
    }

    public String getNama_sekolah() {
        return nama_sekolah;
    }

    public String getKelas() {
        return kelas;
    }

    public String getId_login() {
        return id_login;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getLevel() {
        return level;
    }

    public String getStatus() {
        return status;
    }

    public String getId_unit() {
        return id_unit;
    }

    public String getNama_unit() {
        return nama_unit;
    }

    public String getGambar() {
        return gambar;
    }
}
