package com.dwi.dwiskripsi.model;

public class Unit {

    private String id_unit;
    private String nama_unit;
    private String alamat;
    private String telp;
    private String gambar;
    private String url;
    private String deskripsi;
    private String latitude;
    private String longitude;
    private String id_login;
    private String id_kecamatan;
    private String nama_kecamatan;
    private String username;
    private String password;
    private double jarak;

    public Unit() {

    }

    public Unit(String id_unit, String nama_unit, String alamat, String telp,
                String gambar, String url, String deskripsi, String latitude, String longitude,
                String id_login, String id_kecamatan, String nama_kecamatan, String username, String password) {

        this.id_unit = id_unit;
        this.nama_unit = nama_unit;
        this.alamat = alamat;
        this.telp = telp;
        this.gambar = gambar;
        this.url = url;
        this.deskripsi = deskripsi;
        this.latitude = latitude;
        this.longitude = longitude;
        this.id_login = id_login;
        this.id_kecamatan = id_kecamatan;
        this.nama_kecamatan = nama_kecamatan;
        this.username = username;
        this.password = password;

    }

    public double getJarak() {
        return jarak;
    }

    public void setJarak(double jarak) {
        this.jarak = jarak;
    }

    public String getId_unit() {
        return id_unit;
    }

    public String getNama_unit() {
        return nama_unit;
    }

    public String getAlamat() {
        return alamat;
    }

    public String getTelp() {
        return telp;
    }

    public String getGambar() {
        return gambar;
    }

    public String getUrl() {
        return url;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public String getLatitude() {
        return latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public String getId_login() {
        return id_login;
    }

    public String getId_kecamatan() {
        return id_kecamatan;
    }

    public String getNama_kecamatan() {
        return nama_kecamatan;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }
}
