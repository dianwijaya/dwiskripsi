package com.dwi.dwiskripsi.unit;

import android.content.Intent;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.NestedScrollView;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.dwi.dwiskripsi.App;
import com.dwi.dwiskripsi.ImagePreviewActivity;
import com.dwi.dwiskripsi.Loading;
import com.dwi.dwiskripsi.R;
import com.dwi.dwiskripsi.model.Unit;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.dwi.dwiskripsi.Config.GETUNIT;
import static com.dwi.dwiskripsi.Config.GETUNITBYID;
import static com.dwi.dwiskripsi.Config.ID_LOGIN;
import static com.dwi.dwiskripsi.Config.URL_FOTO_UNIT;

public class ProfileUnitActivity extends AppCompatActivity {

    private TextView Tnama_unit;
    private TextView Talamat;
    private TextView Ttelp;
    private TextView Turl;
    private TextView Tdeskripsi;
    private TextView Tlatitude;
    private TextView Tlongitude;
    private TextView Tusername;
    private TextView Tpassword;
    private TextView Tkecamatan;
    private ImageView ViewGambar;

    private String id_unit;
    private String nama_unit;
    private String alamat;
    private String telp;
    private String gambar;
    private String url;
    private String deskripsi;
    private String latitude;
    private String longitude;
    private String id_login;
    private String username;
    private String password;
    private String nama_kecamatan;

    private Loading load;
    private SwipeRefreshLayout swipe;
    private Button editprofil;

    //maps
    private double lati, longi;
    private MapView mapView;
    private GoogleMap googleMap;
    private NestedScrollView mainScrollView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_unit);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("Profil");

        load = new Loading(this);

        id_login = getIntent().getExtras().getString("id_login");

        editprofil = (Button) findViewById(R.id.editprofil);
        swipe = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh);
        Tnama_unit = (TextView) findViewById(R.id.nama_unit);
        Talamat = (TextView) findViewById(R.id.alamat);
        Ttelp = (TextView) findViewById(R.id.telp);
        Turl = (TextView) findViewById(R.id.url);
        Tdeskripsi = (TextView) findViewById(R.id.deskripsi);
        Tlatitude = (TextView) findViewById(R.id.latitude);
        Tlongitude = (TextView) findViewById(R.id.longitude);
        Tusername = (TextView) findViewById(R.id.username);
        Tpassword = (TextView) findViewById(R.id.password);
        Tkecamatan = (TextView) findViewById(R.id.kecamatan);
        ViewGambar = (ImageView) findViewById(R.id.gambar);



        mainScrollView = (NestedScrollView) findViewById(R.id.scrollView);
        mapView = (MapView) findViewById(R.id.map);
        mapView.onCreate(savedInstanceState);
        mapView.onResume();

        ImageView transparentImageView = (ImageView) findViewById(R.id.transparent_image);

        transparentImageView.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                    case MotionEvent.ACTION_DOWN:
                        mainScrollView.requestDisallowInterceptTouchEvent(true);
                        return false;
                    case MotionEvent.ACTION_UP:
                        mainScrollView.requestDisallowInterceptTouchEvent(false);
                        return true;
                    case MotionEvent.ACTION_MOVE:
                        mainScrollView.requestDisallowInterceptTouchEvent(true);
                        return false;
                    default:
                        return true;
                }
            }
        });

        swipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getUnit();
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        getUnit();
    }

    public void getUnit() {
        load.showpDialog();
        StringRequest jsonReq = new StringRequest(Request.Method.POST, GETUNITBYID, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("ResponseUnit", response.toString());
                if (response != null) {
                    try {
                        JSONObject Jsondata = new JSONObject(response);
                        String status = Jsondata.getString("status");

                            try {
                                JSONArray feedArray = Jsondata.getJSONArray("data");

                                for (int i = 0; i < 1; i++) {
                                    JSONObject data = (JSONObject) feedArray.get(i);

                                    final String id_unit = data.has("id_unit") ? data.getString("id_unit") : "";
                                    final String nama_unit = data.has("nama_unit") ? data.getString("nama_unit") : "";
                                    final String alamat = data.has("alamat") ? data.getString("alamat") : "";
                                    final String telp = data.has("telp") ? data.getString("telp") : "";
                                    final String gambar = data.has("gambar") ? data.getString("gambar") : "";
                                    final String url = data.has("url") ? data.getString("url") : "";
                                    final String deskripsi = data.has("deskripsi") ? data.getString("deskripsi") : "";
                                    final String latitude = data.has("latitude") ? data.getString("latitude") : "";
                                    final String longitude = data.has("longitude") ? data.getString("longitude") : "";
                                    final String id_login = data.has("id_login") ? data.getString("id_login") : "";
                                    final String id_kecamatan = data.has("id_kecamatan") ? data.getString("id_kecamatan") : "";
                                    String nama_kecamatan = data.has("nama_kecamatan") ? data.getString("nama_kecamatan") : "";
                                    final String username = data.has("username") ? data.getString("username") : "";
                                    final String password = data.has("password") ? data.getString("password") : "";

                                    editprofil.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            Intent intent = new Intent(ProfileUnitActivity.this, EditProfileUnitActivity.class);
                                            intent.putExtra("id_unit", id_unit);
                                            intent.putExtra("nama_unit", nama_unit);
                                            intent.putExtra("alamat", alamat);
                                            intent.putExtra("telp", telp);
                                            intent.putExtra("gambar", gambar);
                                            intent.putExtra("url", url);
                                            intent.putExtra("deskripsi", deskripsi);
                                            intent.putExtra("latitude", latitude);
                                            intent.putExtra("longitude", longitude);
                                            intent.putExtra("id_login", id_login);
                                            intent.putExtra("username", username);
                                            intent.putExtra("password", password);
                                            intent.putExtra("id_kecamatan", id_kecamatan);
                                            startActivity(intent);
                                        }
                                    });

                                    Tnama_unit.setText(nama_unit);
                                    Talamat.setText(alamat);
                                    Ttelp.setText(telp);
                                    Turl.setText(url);
                                    Tdeskripsi.setText(deskripsi);
                                    Tlatitude.setText(latitude);
                                    Tlongitude.setText(longitude);
                                    Tusername.setText(username);
                                    Tpassword.setText(password);
                                    Tkecamatan.setText(nama_kecamatan);

                                    if(gambar != null && !gambar.equals("")){
                                        Picasso.with(ProfileUnitActivity.this)
                                                .load(URL_FOTO_UNIT+gambar)
                                                .placeholder(R.drawable.ic_loading)
                                                .error(R.drawable.ic_image)
                                                .into(ViewGambar);
                                    } else {
                                        ViewGambar.setImageResource(R.drawable.ic_image);
                                    }

                                    ViewGambar.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            Intent intent = new Intent(ProfileUnitActivity.this, ImagePreviewActivity.class);
                                            intent.putExtra("url", URL_FOTO_UNIT+gambar);
                                            startActivity(intent);
                                        }
                                    });

                                    try {
                                        lati = Double.parseDouble(latitude);
                                    } catch (NumberFormatException e) {
                                        lati = 0.000;
                                    }

                                    try {
                                        longi = Double.parseDouble(longitude);
                                    } catch (NumberFormatException e) {
                                        longi = 0.000;
                                    }

                                    try {
                                        MapsInitializer.initialize(ProfileUnitActivity.this);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }

                                    mapView.getMapAsync(new OnMapReadyCallback() {
                                        @Override
                                        public void onMapReady(GoogleMap mMap) {
                                            googleMap = mMap;
                                            LatLng lokasi = new LatLng(lati, longi);
                                            googleMap.addMarker(new MarkerOptions().position(lokasi).title(nama_unit).snippet(alamat));
                                            CameraPosition cameraPosition = new CameraPosition.Builder().target(lokasi).zoom(18).build();
                                            googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                                        }
                                    });

                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                load.hidepDialog();
                swipe.setRefreshing(false);
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("ERRORUNIT", error.toString());
                load.hidepDialog();
                swipe.setRefreshing(false);
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("id_login", App.getData(ID_LOGIN));
                return params;
            }
        };
        App.getInstance().addToRequestQueue(jsonReq);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                onBackPressed();
        }
        return (super.onOptionsItemSelected(menuItem));
    }
}
