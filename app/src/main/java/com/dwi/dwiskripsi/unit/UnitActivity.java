package com.dwi.dwiskripsi.unit;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.dwi.dwiskripsi.App;
import com.dwi.dwiskripsi.Loading;
import com.dwi.dwiskripsi.LoginActivity;
import com.dwi.dwiskripsi.R;
import android.support.design.widget.Snackbar;

import static com.dwi.dwiskripsi.Config.USERNAME;

public class UnitActivity extends AppCompatActivity {

    private Button profil, pendaftar;
    private String id_login;
    private Loading load;
    private boolean backPressToExit = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_unit);
        //setTitle(App.getData(USERNAME));
        setTitle("Tempat Les");

        profil = (Button) findViewById(R.id.profil);
        pendaftar = (Button) findViewById(R.id.datapendaftar);

        profil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(UnitActivity.this, ProfileUnitActivity.class);
                intent.putExtra("id_login", id_login);
                startActivity(intent);
            }
        });

        pendaftar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(UnitActivity.this, DaftarPendaftarActivity.class);
                startActivity(intent);
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.logout, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.logout) {
            App.logout();
            Intent intent = new Intent(UnitActivity.this, LoginActivity.class);
            startActivity(intent);
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {

        if (backPressToExit) {
            super.onBackPressed();
            finish();
            return;
        }
        this.backPressToExit = true;
        Snackbar.make(findViewById(R.id.myView), "Tekan sekali lagi untuk keluar.", Snackbar.LENGTH_SHORT).show();
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                backPressToExit = false;
            }
        }, 2000);
    }
}
