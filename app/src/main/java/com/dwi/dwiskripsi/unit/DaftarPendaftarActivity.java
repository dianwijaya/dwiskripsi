package com.dwi.dwiskripsi.unit;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.dwi.dwiskripsi.App;
import com.dwi.dwiskripsi.ImagePreviewActivity;
import com.dwi.dwiskripsi.Loading;
import com.dwi.dwiskripsi.R;
import com.dwi.dwiskripsi.admin.admin.DataAdminActivity;
import com.dwi.dwiskripsi.admin.pendaftar.DataPendaftarActivity;
import com.dwi.dwiskripsi.admin.pendaftar.DetailPendaftarActivity;
import com.dwi.dwiskripsi.admin.pendaftar.EditPendaftarActivity;
import com.dwi.dwiskripsi.admin.pendaftar.TambahPendaftarActivity;
import com.dwi.dwiskripsi.model.Pendaftar;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.dwi.dwiskripsi.Config.GETPENDAFTAR;
import static com.dwi.dwiskripsi.Config.GETPENDAFTARBYID;
import static com.dwi.dwiskripsi.Config.GETPENDAFTARBYID_UNIT;
import static com.dwi.dwiskripsi.Config.HAPUSPENDAFTAR;
import static com.dwi.dwiskripsi.Config.ID_LOGIN;
import static com.dwi.dwiskripsi.Config.TANDAITERIMA;
import static com.dwi.dwiskripsi.Config.URL_FOTO_ADMIN;
import static com.dwi.dwiskripsi.Config.URL_FOTO_PENDAFTAR;

public class DaftarPendaftarActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private PendaftarAdapter adapter;
    private ArrayList<Pendaftar> pendaftarList = new ArrayList<>();
    private SwipeRefreshLayout swipe;
    private Loading load;
    private TextView nodata;
    private SearchView sv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daftar_pendaftar_by_unit);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("Data Siswa");
        sv = (SearchView) findViewById(R.id.seacrh);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        nodata = (TextView) findViewById(R.id.nodata);
        swipe = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh);
        load = new Loading(this);

        adapter = new PendaftarAdapter(pendaftarList);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);

        sv.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                adapter.getFilter().filter(newText);
                return false;
            }
        });

        swipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getPendaftar();
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        getPendaftar();
    }

    public void getPendaftar() {
        load.showpDialog();
        StringRequest jsonReq = new StringRequest(Request.Method.POST, GETPENDAFTARBYID_UNIT, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("ResponsePendaftar", response.toString());
                if (response != null) {
                    try {
                        pendaftarList.clear();
                        JSONObject Jsondata = new JSONObject(response);
                        String status = Jsondata.getString("status");
                        if (status.equals("0")) {
                            nodata.setVisibility(View.VISIBLE);
                            nodata.setText(getString(R.string.nodata));
                        } else {
                            nodata.setVisibility(View.GONE);
                            try {
                                JSONArray feedArray = Jsondata.getJSONArray("data");

                                for (int i = 0; i < feedArray.length(); i++) {
                                    JSONObject data = (JSONObject) feedArray.get(i);

                                    String id_pendaftar = data.has("id_pendaftar") ? data.getString("id_pendaftar") : "";
                                    String nama_lengkap = data.has("nama_lengkap") ? data.getString("nama_lengkap") : "";
                                    String nama_pendek = data.has("nama_pendek") ? data.getString("nama_pendek") : "";
                                    String tempat_lahir = data.has("tempat_lahir") ? data.getString("tempat_lahir") : "";
                                    String tanggal_lahir = data.has("tanggal_lahir") ? data.getString("tanggal_lahir") : "";
                                    String agama = data.has("agama") ? data.getString("agama") : "";
                                    String nama_ortu = data.has("nama_ortu") ? data.getString("nama_ortu") : "";
                                    String alamat = data.has("alamat") ? data.getString("alamat") : "";
                                    String telp = data.has("telp") ? data.getString("telp") : "";
                                    String nama_sekolah = data.has("nama_sekolah") ? data.getString("nama_sekolah") : "";
                                    String kelas = data.has("kelas") ? data.getString("kelas") : "";
                                    String id_login = data.has("id_login") ? data.getString("id_login") : "";
                                    String username = data.has("kelas") ? data.getString("username") : "";
                                    String password = data.has("password") ? data.getString("password") : "";
                                    String level = data.has("level") ? data.getString("level") : "";
                                    String status2 = data.has("status") ? data.getString("status") : "";
                                    String id_unit = data.has("id_unit") ? data.getString("id_unit") : "";
                                    String nama_unit = data.has("nama_unit") ? data.getString("nama_unit") : "";
                                    String gambar = data.has("gambar") ? data.getString("gambar") : "";


                                    Pendaftar a = new Pendaftar(id_pendaftar, nama_lengkap, nama_pendek,
                                            tempat_lahir, tanggal_lahir, agama, nama_ortu, alamat, telp, nama_sekolah,
                                            kelas, id_login, username, password, level, status2, id_unit,nama_unit,gambar);
                                    pendaftarList.add(a);

                                }
                                adapter.notifyDataSetChanged();
                            } catch (JSONException e) {
                                Log.e("ERROJSON", e.toString());
                            }
                        }
                    } catch (JSONException e) {
                        Log.e("ERROJSON", e.toString());
                    }
                }
                load.hidepDialog();
                swipe.setRefreshing(false);
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("ERRORpendaftar", error.toString());
                nodata.setText(getString(R.string.noresponse));
                nodata.setVisibility(View.VISIBLE);
                load.hidepDialog();
                swipe.setRefreshing(false);
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("id_login", App.getData(ID_LOGIN));
                return params;
            }
        };
        App.getInstance().addToRequestQueue(jsonReq);
    }

    private void HapusPendaftar(final String id_pendaftar, final String id_login){
        load.showpDialog();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, HAPUSPENDAFTAR,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("ResponseHapusPendaftar", response);
                        if (response != null) {
                            try {
                                JSONObject Jsondata = new JSONObject(response);
                                String status = Jsondata.getString("status");
                                if (status.equals("0")) {
                                    Toast.makeText(DaftarPendaftarActivity.this, "Maaf, gagal menghapus" , Toast.LENGTH_SHORT);
                                } else {
                                    Toast.makeText(DaftarPendaftarActivity.this, "Berhasil menghapus data :)" , Toast.LENGTH_SHORT);
                                    getPendaftar();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        load.hidepDialog();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("ERRORHAPUSPENDAFTAR", error.toString());
                        load.hidepDialog();
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("id_pendaftar", id_pendaftar);
                params.put("id_login", id_login);

                Log.e("ID_PENDAFTAR", id_pendaftar);
                Log.e("ID_LOGIN", id_login);
                return params;
            }
        };

        App.getInstance().addToRequestQueue(stringRequest);
    }

    private void tandaiterima(final String id_pendaftar){
        load.showpDialog();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, TANDAITERIMA,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("ResponseTerimaPendaftar", response);
                        if (response != null) {
                            try {
                                JSONObject Jsondata = new JSONObject(response);
                                String status = Jsondata.getString("status");
                                if (status.equals("0")) {
                                    Toast.makeText(DaftarPendaftarActivity.this, "Maaf, gagal menerima" , Toast.LENGTH_SHORT);
                                } else {
                                    Toast.makeText(DaftarPendaftarActivity.this, "Berhasil menerima :)" , Toast.LENGTH_SHORT);
                                    getPendaftar();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        load.hidepDialog();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("ERRORTERIMAPENDAFTAR", error.toString());
                        load.hidepDialog();
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("id_pendaftar", id_pendaftar);
                return params;
            }
        };

        App.getInstance().addToRequestQueue(stringRequest);
    }

    private class PendaftarAdapter extends RecyclerView.Adapter<PendaftarAdapter.MyViewHolder> implements Filterable {

        private List<Pendaftar> pendaftarList;
        ArrayList<Pendaftar> filterlist;
        PendaftarFilter filter;

        public class MyViewHolder extends RecyclerView.ViewHolder {
            private TextView Tnama_lengkap, Tnama_pendek, Ttempat_lahir, Ttanggal_lahir,
                    Tagama, Tnama_ortu, Talamat, Ttelp, Tnama_sekolah, Tkelas, Tusername,
                    Tpassword, Tstatus, Tnama_unit;
            private LinearLayout linear;
            private CircleImageView foto;
            private Button konfirmasiterima;

            public MyViewHolder(View view) {
                super(view);

                Tnama_lengkap = (TextView) view.findViewById(R.id.nama_lengkap);
                //Tnama_pendek = (TextView) view.findViewById(R.id.nama_pendek);
                //Ttempat_lahir = (TextView) view.findViewById(R.id.tempat_lahir);
                Ttanggal_lahir = (TextView) view.findViewById(R.id.tanggal_lahir);
                //Tagama = (TextView) view.findViewById(R.id.agama);
                //Tnama_ortu = (TextView) view.findViewById(R.id.nama_ortu);
                //Talamat = (TextView) view.findViewById(R.id.alamat);
                //Ttelp = (TextView) view.findViewById(R.id.telp);
                Tnama_sekolah = (TextView) view.findViewById(R.id.nama_sekolah);
                //Tkelas = (TextView) view.findViewById(R.id.kelas);
                //Tusername = (TextView) view.findViewById(R.id.username);
                //Tpassword = (TextView) view.findViewById(R.id.password);
                //Tstatus = (TextView) view.findViewById(R.id.status);
                Tnama_unit = (TextView) view.findViewById(R.id.nama_unit);
                konfirmasiterima = (Button) view.findViewById(R.id.konfirmasiterima);

                linear = (LinearLayout) view.findViewById(R.id.linear);
                foto = (CircleImageView) view.findViewById(R.id.gambar);
            }
        }

        public PendaftarAdapter(ArrayList<Pendaftar> pendaftarList) {
            this.pendaftarList = pendaftarList;
            this.filterlist = pendaftarList;
        }

        @Override
        public PendaftarAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.data_unit_pendaftar_layout, parent, false);
            return new PendaftarAdapter.MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(final PendaftarAdapter.MyViewHolder holder, final int position) {
            final Pendaftar pendaftar = pendaftarList.get(position);

            holder.Tnama_lengkap.setText(pendaftar.getNama_lengkap());
            //holder.Tnama_pendek.setText(pendaftar.getNama_pendek());
            //holder.Ttempat_lahir.setText(pendaftar.getTempat_lahir());
            holder.Ttanggal_lahir.setText(pendaftar.getTanggal_lahir());
            //holder.Tagama.setText(pendaftar.getAgama());
            //holder.Tnama_ortu.setText(pendaftar.getNama_ortu());
            //holder.Talamat.setText(pendaftar.getAlamat());
            //holder.Ttelp.setText(pendaftar.getTelp());
            holder.Tnama_sekolah.setText(pendaftar.getNama_sekolah());
//            holder.Tkelas.setText(pendaftar.getKelas());
//            holder.Tusername.setText(pendaftar.getUsername());
//            holder.Tpassword.setText(pendaftar.getPassword());
//            holder.Tstatus.setText(pendaftar.getStatus());
            holder.Tnama_unit.setText(pendaftar.getNama_unit());

            holder.konfirmasiterima.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AlertDialog.Builder builder1 = new AlertDialog.Builder(DaftarPendaftarActivity.this);
                    builder1.setMessage("Apakah Anda yakin menerima siswa ini ?");
                    builder1.setCancelable(false);

                    builder1.setPositiveButton("Ya",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    tandaiterima(pendaftar.getId_pendaftar());
                                    dialog.cancel();
                                }
                            });

                    builder1.setNegativeButton("Tidak",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });

                    AlertDialog alert11 = builder1.create();
                    alert11.show();
                }
            });

            if(pendaftar.getGambar() != null && !pendaftar.getGambar().equals("")){
                Picasso.with(DaftarPendaftarActivity.this)
                        .load(URL_FOTO_PENDAFTAR+pendaftar.getGambar())
                        .placeholder(R.drawable.ic_loading)
                        .error(R.drawable.ic_noimage)
                        .into(holder.foto);
            } else {
                holder.foto.setImageResource(R.drawable.ic_noimage);
            }

            holder.foto.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(DaftarPendaftarActivity.this, ImagePreviewActivity.class);
                    intent.putExtra("url", URL_FOTO_PENDAFTAR+pendaftar.getGambar());
                    startActivity(intent);
                }
            });

            holder.linear.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(DaftarPendaftarActivity.this, DetailPendaftarActivity.class);
                    intent.putExtra("id_pendaftar", pendaftar.getId_pendaftar());
                    intent.putExtra("nama_lengkap", pendaftar.getNama_lengkap());
                    intent.putExtra("nama_pendek", pendaftar.getNama_pendek());
                    intent.putExtra("tempat_lahir", pendaftar.getTempat_lahir());
                    intent.putExtra("tanggal_lahir", pendaftar.getTanggal_lahir());
                    intent.putExtra("agama", pendaftar.getAgama());
                    intent.putExtra("nama_ortu", pendaftar.getNama_ortu());
                    intent.putExtra("alamat", pendaftar.getAlamat());
                    intent.putExtra("telp", pendaftar.getTelp());
                    intent.putExtra("nama_sekolah", pendaftar.getNama_sekolah());
                    intent.putExtra("kelas", pendaftar.getKelas());
                    intent.putExtra("username", pendaftar.getUsername());
                    intent.putExtra("password", pendaftar.getPassword());
                    intent.putExtra("id_login", pendaftar.getId_login());
                    intent.putExtra("status", pendaftar.getStatus());
                    intent.putExtra("nama_unit", pendaftar.getNama_unit());
                    intent.putExtra("gambar", pendaftar.getGambar());
                    intent.putExtra("from", "unit");

                    startActivity(intent);
                }
            });

            /*holder.linear.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(DaftarPendaftarActivity.this);

                    LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    View dialogView = inflater.inflate(R.layout.dialog_edit_delete_layout, null);
                    dialogBuilder.setView(dialogView);

                    Button edit = (Button) dialogView.findViewById(R.id.edit);

                    //Hidde tombol edit
                    edit.setVisibility(View.GONE);

                    Button hapus = (Button) dialogView.findViewById(R.id.hapus);

                    final AlertDialog alertDialog = dialogBuilder.create();
                    alertDialog.setCancelable(true);

                    edit.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            alertDialog.dismiss();
                            Intent intent = new Intent(DaftarPendaftarActivity.this, EditPendaftarActivity.class);
                            intent.putExtra("id_pendaftar", pendaftar.getId_pendaftar());
                            intent.putExtra("nama_lengkap", pendaftar.getNama_lengkap());
                            intent.putExtra("nama_pendek", pendaftar.getNama_pendek());
                            intent.putExtra("tempat_lahir", pendaftar.getTempat_lahir());
                            intent.putExtra("tanggal_lahir", pendaftar.getTanggal_lahir());
                            intent.putExtra("agama", pendaftar.getAgama());
                            intent.putExtra("nama_ortu", pendaftar.getNama_ortu());
                            intent.putExtra("alamat", pendaftar.getAlamat());
                            intent.putExtra("telp", pendaftar.getTelp());
                            intent.putExtra("nama_sekolah", pendaftar.getNama_sekolah());
                            intent.putExtra("kelas", pendaftar.getKelas());
                            intent.putExtra("username", pendaftar.getUsername());
                            intent.putExtra("password", pendaftar.getPassword());
                            intent.putExtra("id_login", pendaftar.getId_login());
                            intent.putExtra("status", pendaftar.getStatus());
                            intent.putExtra("id_unit", pendaftar.getId_unit());
                            intent.putExtra("gambar", pendaftar.getGambar());

                            startActivity(intent);
                        }
                    });

                    hapus.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            alertDialog.dismiss();
                            AlertDialog.Builder builder1 = new AlertDialog.Builder(DaftarPendaftarActivity.this);
                            builder1.setMessage("Apakah Anda yakin ingin menghapus ini ?");
                            builder1.setCancelable(false);

                            builder1.setPositiveButton("Ya",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            HapusPendaftar(pendaftar.getId_pendaftar(), pendaftar.getId_login());
                                            dialog.cancel();
                                        }
                                    });

                            builder1.setNegativeButton("Tidak",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.cancel();
                                        }
                                    });

                            AlertDialog alert11 = builder1.create();
                            alert11.show();
                        }
                    });


                    if (!alertDialog.isShowing()) {
                        alertDialog.show();
                    }

                    return false;
                }
            });*/

            if(pendaftar.getStatus().equalsIgnoreCase("aktif")){
                holder.konfirmasiterima.setVisibility(View.GONE);
            } else {
                holder.konfirmasiterima.setVisibility(View.VISIBLE);
            }

        }

        @Override
        public int getItemCount() {
            return pendaftarList.size();
        }

        @Override
        public Filter getFilter() {
            if(filter == null){
                filter = new PendaftarFilter(filterlist, this);
            }
            return filter;
        }
    }

    private class PendaftarFilter extends Filter {

        private PendaftarAdapter adapter;
        private ArrayList<Pendaftar> filterList;

        public PendaftarFilter(ArrayList<Pendaftar> filterList, PendaftarAdapter adapter){
            this.filterList = filterList;
            this.adapter = adapter;
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();
            //check constraint
            if(constraint != null && constraint.length() > 0){
                ArrayList<Pendaftar> filteredPendaftar = new ArrayList<>();

                for (int i = 0; i < filterList.size(); i++){
                    if (filterList.get(i).getNama_lengkap().toUpperCase().contains(constraint.toString().toUpperCase())
                            | filterList.get(i).getNama_lengkap().toUpperCase().contains(constraint.toString().toUpperCase())
                            ){
                        filteredPendaftar.add(filterList.get(i));
                    }
                }
                results.count = filteredPendaftar.size();
                results.values = filteredPendaftar;
            } else {
                results.count = filterList.size();
                results.values = filterList;
            }
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            adapter.pendaftarList = (ArrayList<Pendaftar>) results.values;
            if(results.count > 0){
                nodata.setVisibility(View.GONE);
            } else {
                nodata.setText("Data tidak ditemukan");
                nodata.setVisibility(View.VISIBLE);
            }
            adapter.notifyDataSetChanged();

        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                onBackPressed();
        }
        return (super.onOptionsItemSelected(menuItem));
    }
}
