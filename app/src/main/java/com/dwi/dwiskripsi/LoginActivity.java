package com.dwi.dwiskripsi;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.dwi.dwiskripsi.admin.AdminActivity;
import com.dwi.dwiskripsi.unit.UnitActivity;
import com.dwi.dwiskripsi.pendaftar.PendaftarActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.dwi.dwiskripsi.Config.ID_LOGIN;
import static com.dwi.dwiskripsi.Config.LEVEL;
import static com.dwi.dwiskripsi.Config.LOGIN;
import static com.dwi.dwiskripsi.Config.USERNAME;

public class LoginActivity extends AppCompatActivity {

    private EditText username, password;
    private Button masuk;//, daftar;
    private Loading load;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("Login");

        load = new Loading(this);

        username = (EditText) findViewById(R.id.username);
        password = (EditText) findViewById(R.id.password);
        masuk = (Button) findViewById(R.id.masuk);
        /*daftar = (Button) findViewById(R.id.daftar);

        daftar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, SignUpActivity.class);
                startActivity(intent);
            }
        });*/

        masuk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(username.getText().toString().isEmpty()){
                    username.setError("harus diisi.");
                } else if(password.getText().toString().isEmpty()){
                    password.setError("harus diisi.");
                } else {
                    login();
                }
            }
        });
    }

    private void login(){
        load.showpDialog();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, LOGIN,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("responLogin", response);
                        if (response != null) {
                            try {
                                JSONObject Jsondata = new JSONObject(response);
                                String status = Jsondata.getString("status");
                                if (status.equals("0")) {
                                    Toast.makeText(LoginActivity.this, "username / password salah", Toast.LENGTH_SHORT).show();
                                } else {
                                    try {
                                        JSONArray feedArray = Jsondata.getJSONArray("data");

                                        for (int i = 0; i < feedArray.length(); i++) {
                                            JSONObject data = (JSONObject) feedArray.get(i);

                                            String id_login = data.has("id_login") ? data.getString("id_login") : "";
                                            String username = data.has("username") ? data.getString("username") : ""; String password = data.has("password") ? data.getString("password") : "";
                                            String level = data.has("level") ? data.getString("level") : "";

                                            if(level.equalsIgnoreCase("admin")){
                                                App.saveData(ID_LOGIN, id_login);
                                                App.saveData(LEVEL, level);
                                                App.saveData(USERNAME, username);
                                                Intent intent = new Intent(LoginActivity.this, AdminActivity.class);
                                                startActivity(intent);
                                                finish();
                                            } else if(level.equalsIgnoreCase("pendaftar")){
                                                App.saveData(ID_LOGIN, id_login);
                                                App.saveData(LEVEL, level);
                                                App.saveData(USERNAME, username);
                                                Intent intent = new Intent(LoginActivity.this, PendaftarActivity.class);
                                                startActivity(intent);
                                                finish();
                                            } else if(level.equalsIgnoreCase("unit")){
                                                App.saveData(ID_LOGIN, id_login);
                                                App.saveData(LEVEL, level);
                                                App.saveData(USERNAME, username);
                                                Intent intent = new Intent(LoginActivity.this, UnitActivity.class);
                                                startActivity(intent);
                                                finish();
                                            }
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            } catch (JSONException e) {
                                Log.e("ErroJSon", e.toString());
                            }
                        }
                        load.hidepDialog();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("responLogin", error.toString());
                        load.hidepDialog();
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("username", username.getText().toString());
                params.put("password", password.getText().toString());
                return params;
            }
        };
        App.getInstance().addToRequestQueue(stringRequest);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                Intent intent = new Intent(LoginActivity.this, MapsUnitActivity.class);
                startActivity(intent);
                finish();
        }
        return (super.onOptionsItemSelected(menuItem));
    }

    @Override
    public void onBackPressed() {

        Intent intent = new Intent(LoginActivity.this, MapsUnitActivity.class);
        startActivity(intent);
        finish();
    }
}
